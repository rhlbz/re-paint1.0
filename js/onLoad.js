/* PRESA DEGLI ELEMENTI DALLA PAGINA */
var graphicContext = document.getElementById("graphicContext");
// myCanvas 
var c=document.getElementById("myCanvas");
// contesto 2D di myCanvas
var ctx=c.getContext("2d");
// palette
var p = document.getElementById("palette");
// contesto 2D di palette
var palette = p.getContext("2d");
// offsets dei contesti 2D di myCanvas e palette
var paletteOffsetLeft = palette.canvas.offsetLeft;
var canvasOffsetLeft = ctx.canvas.offsetLeft;
var canvasOffsetTop = ctx.canvas.offsetTop;
// flag per il disegno o altre operazioni sui canvas che potrebbero essere utili
var offsetX;
var offsetY;
var posX; 
var posY; 
// indice del colore selezionato sulla palette di colori presente nel canvas palette
var selectedIndexColor;
// aggiunta dell'evento keydown sul canvas myCanvas
c.addEventListener('keydown',onKeyPress,false);

/* ELEMENTI AGGIUNTIVI PER IL DISEGNO */

// array dei colori
var colors = new Array();
// elemento grafico scelto che si vuol disegnare/aggiungere al canvas 
var graphic = undefined;

// flags per riempimento e colorazione del bordo
var setFill = false; 

// GRIGLIA
// flag per settare il disegno della griglia o meno 
var setGrid = false;   

/* CONTENITORE DI TUTTI GLI ELEMENTI DISEGNATI SUL CANVAS myCanvas */
// {['type' : type , 'element': element]}, {['type': type1, 'element' : element1 ]}, {...}
var GraphicElements = new Array();
// indice dell'elemento selezionato, presente in GraphicElements
var selectedItem;

/* STORIA */
var RepaintHistory = new Array();

/* FUNZIONALITA' DELLA PAGINA HTML */

//popolamento select font
populateFontSelect();
//visualizzazione scelte palette di color
drawPaletteColors();

/* CREAZIONE DEGLI ELEMENTI GRAFICI DA AGGIUNGERE AL CANVAS myCanvas */

// cerchi per la tranform delle figure sul canvas
var circleHookTop = new Circle();
var circleHookBottom = new Circle();

// RETTANGOLO
var rect = new Rect();
var rects = new Array();
// flag per rettangolo
var dragRect = false; 
var isinRect = false;
var transformRectTop = false;
var transformRectBottom = false;

// ELLISSE
var ellipse = new Ellipse();
var ellipses = new Array();
// flag per ellisse
var dragEllipse = false; 
var isinEllipse = false;
var transformEllipseTop = false;
var transformEllipseBottom = false;

// LINEA
var line = new Line();
var lines = new Array();
// flag per linea
var dragLine = false;
var isinLine = false;
var transformLineTop = false;
var transformLineBottom = false;

// CERCHIO
var circle = new Circle();
var circles = new Array();
// flag per cerchio
var dragCircle = false;
var isinCircle = false;
var transformCircleTop = false;
var transformCircleBottom = false;

// STELLA
var star = new Star();
var stars = new Array();
// flag per cerchio
var dragStar = false;
var isinStar = false;
var transformStarTop = false;
var transformStarBottom = false;

// MATITA e PENNELLO
var pencil = new Pencil();
var pencils = new Array();
// flag per matita
var dragPencil = false;
var isinPencil = false;

// TESTO
var text=new Text();
var texts=new Array();
// flag per testo
var dragText=false;
var isinText=false;
var bold = false;
var italic = false;
var underline = false;

var RepaintHistory;

var indexBackgroundColor = undefined;

var regular = false;

$("#myCanvas").css("display", "none");
$("#editText").css("display", "none");
$("#menuSection").css("display", "none");
$("#dim").css("display", "none");
$("#labelCanvasDimension").css("display", "none");
$("#labelCanvasDimension").css("text-align", "center");
var imageName;
var width;
var height;
var copy;
setActiveButton();
changeActiveMenuTool();


