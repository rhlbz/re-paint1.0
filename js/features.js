/////////////////////////////////// VARIE FUNZIONI
function onSelect(){
    graphic = undefined;
    selectedItem = undefined;
    onClickNoColor();
    invalidatePaletteColors();
    document.body.style.cursor = 'default';   
    
    onCheckRect();
    onCheckEllipse();
    onCheckLine();
    onCheckCircle();
    onCheckText();
    onCheckStar();
    invalidate();
}

function onDrawRect(){
    graphic = 'rect';
    setFill = false;
}

function onDrawEllipse(){
    graphic = 'ellipse';
    setFill = false;
}

function onDrawLine(){
    graphic = 'line';
    setFill = false;
}

function onDrawCircle(){
    graphic = 'circle';
    setFill = false;
}

function onDrawStar(){
    graphic = 'star';
    setFill = false;
}

function onDrawBrush(){
    selectedItem = undefined;
    graphic = 'brush';
    $("#setDimensionOfBrush").css("display", "inline");
}

function onDrawPencil(){
    selectedItem = undefined;
    graphic = 'pencil';
    $("#setDimensionOfPencil").css("display", "inline");
}

function onDrawBezier(){
    graphic = 'bezier';
}

function onDrawQuadraticCurve(){
    graphic = 'quadraticCurve';
}

function onDrawText(){
    graphic = 'text';
}

function onClickFill(){
    if ( !setFill ){
        setFill = true;
    } else {
        setFill = false;
    }
}

function onClickNoColor(){
    if ( selectedIndexColor != undefined ) {
        colors[selectedIndexColor].setBorder(false);
        selectedIndexColor = undefined;
        invalidatePaletteColors();
    }
}

function onClickGrid(){
    if ( !setGrid ){
        setGrid = true;
        invalidate();
    } else {
        setGrid = false;
        invalidate();
    }
}

function onSetColor(){
    colorsPalette($("#chooseColors option:selected").val());
    $("#chooseColors").change(function(){
        colorsPalette($("#chooseColors option:selected").val());
        invalidatePaletteColors();
    });
}

function onCheckRect(){
    if ( rects.length > 0 ){
        for ( var i = 0; i < rects.length; i++ ){
            if ((rects[i].Contains(event.offsetX, event.offsetY))){
                graphic='rect';
            }
        }
    }
}

function onCheckEllipse(){
    if ( ellipses.length > 0 ){
        for ( var i = 0; i < ellipses.length; i++ ){
            if ( ellipses[i].Contains(event.offsetX, event.offsetY)){
                graphic='ellipse';
            }
        }
    }
}

function onCheckLine(){
    if ( lines.length > 0 ){
        for ( var i = 0; i < lines.length; i++ ){
            if ( lines[i].Contains(event.offsetX, event.offsetY)){
                graphic='line';
            }
        }
    }
}

function onCheckCircle(){
    if ( circles.length > 0 ){
        for ( var i = 0; i < circles.length; i++ ){
            if ( circles[i].Contains(event.offsetX, event.offsetY)){
                graphic='circle';
            }
        }
    }
}

function onCheckStar(){
    if ( stars.length > 0 ){
        for ( var i = 0; i < stars.length; i++ ){
            if ( stars[i].Contains(event.offsetX, event.offsetY)){
                graphic='star';
            }
        }
    }
}

function onCheckText(){
    if ( texts.length > 0 ){
        for ( var i = 0; i < texts.length; i++ ){
            if ( texts[i].Contains(event.offsetX, event.offsetY)){
                graphic='text';
            }
        }
    }
}


function invalidate(){ /////FUNZIONE INVALIDATE
    ctx.save();
    ctx.clearRect(ctx.canvas.clientTop - 5, ctx.canvas.clientLeft - 5, ctx.canvas.clientWidth + 5, ctx.canvas.clientHeight + 5);
    if ( indexBackgroundColor != undefined ){
        ctx.fillStyle = colors[indexBackgroundColor].getColor();
        ctx.fillRect(ctx.canvas.clientTop - 5, ctx.canvas.clientLeft - 5, ctx.canvas.clientWidth + 5, ctx.canvas.clientHeight + 5);
    }
    if ( setGrid ){
        drawGrid();
    }

    var lastIndexRepaintHistory =  RepaintHistory.length-1;
    var lastElementRepaintHistory = RepaintHistory[lastIndexRepaintHistory];
    if ( lastElementRepaintHistory != undefined ){
        for ( var i = 0; i < lastElementRepaintHistory.length; i++ ){
            if ( lastElementRepaintHistory[i].type == 'rect'){
                drawRect(ctx,lastElementRepaintHistory[i].element);
            } else if ( lastElementRepaintHistory[i].type == 'ellipse'){
                drawEllipse(ctx, lastElementRepaintHistory[i].element);
            } else if ( lastElementRepaintHistory[i].type == 'line'){
                drawLine(ctx, lastElementRepaintHistory[i].element);
            } else if (lastElementRepaintHistory[i].type == 'circle'){
                drawCircle(ctx, lastElementRepaintHistory[i].element);
            } else if ( lastElementRepaintHistory[i].type == 'star'){
                drawStar(ctx, lastElementRepaintHistory[i].element);
            } else if ( lastElementRepaintHistory[i].type == 'pencil'){
                drawPencil(ctx, lastElementRepaintHistory[i].element);
            } else if ( lastElementRepaintHistory[i].type == 'text'){
                if ( lastElementRepaintHistory[i].element.getText() != '' ){
                    drawText(ctx, lastElementRepaintHistory[i].element);
                } else {
                    lastElementRepaintHistory.splice(i, 1); 
                    GraphicElements.splice(GraphicElements.length-1, 1);
                }
            }
        }
    }
    ctx.restore();

}

//DISEGNO GRIGLIA
function drawGrid(){
    var padding = 0;
    var gridWidth = c.getAttribute("width");
    var gridHeight = c.getAttribute("height"); 
    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.setLineDash([]);
    for (var i = 0; i <= gridWidth; i += 10) {
        ctx.moveTo(i, padding);
        ctx.lineTo(i, gridHeight + padding);
    }
    for (var i = 0; i <= gridHeight; i += 10) {
        ctx.moveTo(padding, i);
        ctx.lineTo(gridWidth + padding, i);
    }
    ctx.strokeStyle = "lightgray";
    ctx.stroke();
    ctx.closePath();
}

//DISEGNO PALETTE 
function drawPaletteColors(){
    invalidatePaletteColors();
    colors = new Array();
    for ( var i = 0; i < 10 ; i++){
        var color = new Rect();
        color.setX(i*25 + 5);
        color.setY(5);
        color.setWidth(20);
        color.setHeight(20);
        color.setFill(true);
        color.setBorder(false);
        color.setBorderColor("whitesmoke"); 
        color.setColor(Colors[i]);
        drawRectPalette(palette, color);
        colors.push(color);
    }
    var x = 0;
    for ( var i = 10; i < 20 ; i++){
        var color = new Rect();
        color.setX(x * 25 + 5);
        color.setY(30);
        color.setWidth(20);
        color.setHeight(20);
        color.setFill(true);
        color.setBorder(false);
        color.setBorderColor("whitesmoke"); 
        color.setColor(Colors[i]);
        drawRectPalette(palette, color);
        colors.push(color);
        x++;
    }
    var x = 0;
    for ( var i = 20; i < 30 ; i++){
        var color = new Rect();
        color.setX(x * 25 + 5);
        color.setY(55);
        color.setWidth(20);
        color.setHeight(20);
        color.setFill(true);
        color.setBorder(false);
        color.setBorderColor("whitesmoke"); 
        color.setColor(Colors[i]);
        drawRectPalette(palette, color);
        colors.push(color);
        x++;
    }
}

//POPOLAMENTO SELECT FONT
function populateFontSelect(){
    var selectFont = document.getElementById("inputTextFamily");
    for ( var i = 0; i < Fonts.length; i++ ){
        var option = document.createElement('OPTION');
        option.setAttribute('value', Fonts[i]);
        option.setAttribute('style', 'font-family:'+ Fonts[i]+'');
        var textOption = document.createTextNode(Fonts[i]);
        option.appendChild(textOption);
        selectFont.appendChild(option);
        
    }
}

//FUNZIONI DI RIMOZIONE
function removeSelectedGraphicElement(){
    if ( selectedItem == undefined ){
        alert("Seleziona l'elemento che vuoi eliminare");
    } else {
        //RepaintHistory[RepaintHistory.length].splice(selectedItem,1);
        GraphicElements.splice(selectedItem, 1);
        var tmp = copyGraphicElements(GraphicElements);
        RepaintHistory.push(tmp);
        invalidate();
        selectedItem = undefined;
        graphic = undefined;
        $("#setDimensionOfBrush").css("display", "none");
        $("#setDimensionOfPencil").css("display", "none");
        $("#editText").css("display", "none");
    }
}

function removeLastElementOfParticulatType(type){
    if (GraphicElements.length >= 0 ){
        var i = GraphicElements.length - 1;
        var NotFind = true;
        while ( NotFind && i >= 0 ){
            if ( GraphicElements[i].type == type ){
                console.log(GraphicElements[i]);
                GraphicElements.splice(i, 1);
                var tmp = copyGraphicElements(GraphicElements);
                RepaintHistory.push(tmp);
                invalidate();
                //invalidate();
                NotFind = false;
            } else {
                i--;
            }
        }
    }
}

function removeLastElement(){
    if (GraphicElements.length >= 0 ){
        GraphicElements.splice(GraphicElements.lenght, 1);
    }
}

function removePencil(){
    if (GraphicElements.length >= 0 ){
        for ( var i = 0; i < GraphicElements.length; i++ ){
            if (GraphicElements[i].type == 'pencil'){
                pencils = new Array();
                GraphicElements.splice(i, 1);
                invalidate();
            }
        }
    }
}


function onSaveImage(){
    onSelect();
    setGrid = false;
    invalidate();
    c.toBlob(function(blob){
        if ( imageName == "" )
            saveAs(blob, "repaint-image.png");
        else 
            saveAs(blob, imageName+".png");
    });
}

function onClickText()
{
    $("#setupText").css("position", "fixed");
    $("#setupText").css("display", "inline");
    $("#setupText").css("z-index", "11111");
    $("#setupText").css("margin-left", $(document).width() / 4.5);
    $("#setupText").css("margin-top", $(document).height() / 3);
    $("#setupText").css("width", $(document).width() / 2);
    
    if ( selectedItem != undefined ){
        $("#setText").css("display", "none");
        $("#changeText").css("display", "inline");
        $("#inputText").val(GraphicElements[selectedItem].element.getText());
        $("#inputTextSize").val(GraphicElements[selectedItem].element.getFontSize());
        $("#inputTextFamily option").each(function(){
            if ( $(this).val() == GraphicElements[selectedItem].element.getFontFamily()){
                $(this).attr("selected", "selected");
            }
        })
    } else {
        $("#changeText").css("display", "none");
        $("#setText").css("display", "inline");
    }
    $("#topElement").css("opacity", "0.5");
    $("#centerElement").css("opacity", "0.5");
    $("#myCanvas").css("opacity", "0.5");
    $("#graphicContext").css("opacity", "0.5");
    $("#dim").css("opacity", "0.5");
}

function onClickInfo(){
    var widthInfo = $(document).width() - 100;
    $("#dialog").css("position", "fixed");
    $("#dialog").css("display", "inline");
    $("#dialog").css("z-index", "11111");
    $("#dialog").css("margin-left", 100);
    $("#dialog").css("margin-top", 50);
    $("#dialog").css("width",  $(document).width() - 200);
    $("#dialog").css("height", $(document).height() - 150);
    if ( (( $(document).width() - 200 ) < 1000) || ( ( $(document).height() - 200 ) < 500 ) ) {
        $("#scrollingInfo").css("overflow-y", "scroll");
        $("#scrollingInfo").css("height", "500");
        $("#scrollingShortcuts").css("overflow-y", "scroll");
        $("#scrollingShortcuts").css("height", "500");
    } else {
        $("#scrollingInfo").css("overflow-y", "hidden");
        $("#scrollingShortcuts").css("overflow-y", "hidden");
    }
    $("#topElement").css("opacity", "0.5");
    $("#centerElement").css("opacity", "0.5");
    $("#myCanvas").css("opacity", "0.5");
    $("#graphicContext").css("opacity", "0.5");
    $("#tools").css("opacity", "0.5");
    $("#dim").css("opacity", "0.5");
}

function onCloseHelp(){
    $("#info").removeClass("active");
    $("#dialog").css("display", "none");
    $("#topElement").css("opacity", "1");
    $("#centerElement").css("opacity", "1");
    $("#myCanvas").css("opacity", "1");
    $("#colorPalette").css("opacity", "1");
    $("#graphicContext").css("opacity", "1");
    $("#tools").css("opacity", "1");
    $("#dim").css("opacity", "1");
}

function onCloseSetupText(){
    invalidate();
    $("#setupText").css("display", "none");
    $("#topElement").css("opacity", "1");
    $("#centerElement").css("opacity", "1");
    $("#myCanvas").css("opacity", "1");
    $("#colorPalette").css("opacity", "1");
    $("#graphicContext").css("opacity", "1");
    $("#tools").css("opacity", "1");
    $("#dim").css("opacity", "1");
}

function startRepaint(choice){

    imageName = $("#newNameImage").val();
    if ( imageName == "") {
        imageName = "repaint-image";
    }
    switch(choice){
        case 'default':
            width = $(document).width() - 100;
            height = $(document).height() - 160;
        break;
        case 'bigrect':
            width = $(document).width() - 250;
            height =  $(document).height() - 200;
        break;
        case 'mediumrect':
            width = $(document).width() - 300;
            height =  $(document).height() - 250;
        break;
        case 'smallrect':
            width = $(document).width() - 400;
            height =  $(document).height() - 350;
        break;
        case 'bigsquare':
            width =  $(document).height() - 150;
            height =  $(document).height() - 150;
        break;
        case 'mediumsquare':
            width =  $(document).height() - 200;
            height =  $(document).height() - 200;
        break;
        case 'smallsquare':
            width =  $(document).height() - 350;
            height =  $(document).height() - 350;
        break;
    }
       
    $("#myCanvas").attr("width", width);
    $("#myCanvas").attr("height", height);
    $("#canvasDimension").text(width +"px X "+height+"px");
    $("#menuSection").css("display", "inline");
    $("#myCanvas").css("display", "inline");
    $("#dim").css("display", "inline");
    $("#labelCanvasDimension").css("display", "inline");
    $("#preCanvas").css("display", "none");
    $("#nameImage").text(imageName);

}

function createNewRepaintImage(){
    document.location.reload();

}

function copySelectedItem(){
    if ( selectedItem != undefined ){
        copy = GraphicElements[selectedItem];
        selectedItem = undefined;        
    }
}


function pasteSelectedItem(){
    if ( copy != undefined ){
        var copyType = copy.type;
        switch(copyType){
            case 'rect':
                paste = new Rect();
                paste.setX(copy.element.getX()+5);
                paste.setY(copy.element.getY()+5);
                paste.setWidth(copy.element.getWidth());
                paste.setHeight(copy.element.getHeight());
                paste.setBorder(copy.element.getBorder());
                paste.setFill(copy.element.getFill());
                paste.setColor(copy.element.getColor());
                paste.setBorderColor(copy.element.getBorderColor());
                paste.setRotate(copy.element.getRotate());
                rects.push(paste);
            break;
            
            case 'ellipse':
                paste = new Ellipse();
                paste.setFirstX(copy.element.getFirstX());
                paste.setFirstY(copy.element.getFirstY());
                paste.setSecondX(copy.element.getSecondX());
                paste.setSecondY(copy.element.getSecondX());
                paste.setX(copy.element.getX()+5);
                paste.setY(copy.element.getY()+5);
                paste.setRadiusX(copy.element.getRadiusX());
                paste.setRadiusY(copy.element.getRadiusY());
                paste.setRotation(copy.element.getRotation());
                paste.setStartAngle(copy.element.getStartAngle());
                paste.setEndAngle(copy.element.getEndAngle());
                paste.setColor(copy.element.getColor());
                paste.setBorderColor(copy.element.getBorderColor());
                paste.setFill(copy.element.getFill());
                paste.setRotate(copy.element.getRotate());
                ellipses.push(paste);
            break;

            case 'circle':
                paste = new Circle();
                paste.setX(copy.element.getX() + 5);
                paste.setY(copy.element.getY() + 5);
                paste.setFirstX(copy.element.getFirstX());
                paste.setFirstY(copy.element.getFirstY());
                paste.setSecondX(copy.element.getSecondX());
                paste.setSecondY(copy.element.getSecondY());
                paste.setRadius(copy.element.getRadius());
                paste.setStartAngle(copy.element.getStartAngle());
                paste.setColor(copy.element.getColor());
                paste.setBorder(copy.element.getBorder());
                paste.setBorderColor(copy.element.getBorderColor());
                paste.setFill(copy.element.getFill());
                circles.push(paste);
            break;

            case 'line':
                paste = new Line();
                paste.setStartX(copy.element.getStartX()+5);
                paste.setStartY(copy.element.getStartY() +5);
                paste.setCX(copy.element.getCX());
                paste.setCY(copy.element.getCY());
                paste.setEndX(copy.element.getEndX()+5);
                paste.setEndY(copy.element.getEndY()+5);
                paste.setColor(copy.element.getColor());
                lines.push(paste);
            break;

            case 'star':
                paste = new Star();
                paste.setX(copy.element.getX());
                paste.setY(copy.element.getY());
                paste.setWidth(copy.element.getWidth());
                paste.setHeight(copy.element.getHeight());
                paste.setCX(copy.element.getCX() + 5);
                paste.setCY(copy.element.getCY() + 5);
                paste.setSpikes(copy.element.getSpikes());
                paste.setOuterRadius(copy.element.getOuterRadius());
                paste.setInnerRadius(copy.element.getInnerRadius());
                paste.setBorder(copy.element.getBorder());
                paste.setFill(copy.element.getFill());
                paste.setColor(copy.element.getColor());
                paste.setBorderColor(copy.element.getBorderColor());
                paste.setRotate(copy.element.getRotate());
                paste.setXContains(copy.element.getXContains()+5);
                paste.setYContains(copy.element.getYContains()+5);
                paste.setWidthContains(copy.element.getWidthContains()+5);
                paste.setHeightContains(copy.element.getHeightContains()+5);
                stars.push(paste);
            break;

            case 'text':
                paste = new Text();
                paste.setText(copy.element.getText());
                paste.setX(copy.element.getX() +5);
                paste.setY(copy.element.getY() +5);
                paste.setColor(copy.element.getColor());
                paste.setBorder(copy.element.getBorder());
                paste.setRotate(copy.element.getRotate());
                paste.setBold(copy.element.getBold());
                paste.setItalic(copy.element.getItalic());
                paste.setUnderline(copy.element.getUnderline());
                paste.setmaxWidth(copy.element.getmaxWidth());
                paste.setfontSize(copy.element.getfontSize());
                paste.setFontFamily(copy.element.getFontFamily());
                paste.setFontStyle(copy.element.getFontSize(), copy.element.getFontFamily());
                paste.setWidth(copy.element.getWidth());
                paste.setHeight(copy.element.getHeight());
                texts.push(paste);
            break;
        };
        GraphicElements.push({'type': copyType, 'element': paste});
        var tmp = copyGraphicElements(GraphicElements);
        RepaintHistory.push(tmp);
        invalidate();
        copy = undefined;
        paste = undefined;
    }
}

function setActiveButton(){
    $('.elements li').each(function(){

        if ( $(this).attr('id') == 'fill' ){
            if ( setFill == true ){
                $(this).addClass('active');  
                $("#myCanvas").css("cursor", "url(./img/fillPaint.png), auto");          
            } else {
                $(this).removeClass('active');
                $("#myCanvas").css("cursor", "default");
            }
        }
        if ( $(this).attr("id") == graphic ){
            if ( setFill == true ){
                $("#fill").addClass("active");
            }   
            $(this).addClass("active");
        } else {
            $(this).removeClass("active");
        }

    });
}


function changeActiveMenuTool() {
    $('.elements li').click(function() {
        
        if ( $(this).attr('id') == 'fill' ){
            if ( setFill == true ){
                $(this).addClass('active');  
                $("#myCanvas").css("cursor", "url(./img/fillPaint.png), auto");          
            } else {
                $(this).removeClass('active');
                $("#myCanvas").css("cursor", "default");
            }
        } else if ( $(this).attr('id') == 'showGrid' ){
            if ( setGrid == true ){
                $(this).addClass('active');          
            } else {
                $(this).removeClass('active');
            }
        } else {
            invalidate();
            if ( $(this).attr('id') == 'createNewImage' ){
                $(this).addClass(''); 
            } else if ( $(this).attr('id') == 'saveImageAsPng' ){
                $(this).addClass(''); 
            } else if ( $(this).attr('id') == 'rotateLeft' ){
                $(this).addClass('');
            } else if ( $(this).attr('id') == 'rotateRight' ){
                $(this).addClass('');
            } else if ( $(this).attr('id') == 'trash' ){
                $(this).addClass(''); 
            } else if ( $(this).attr('id') == 'paste' ){
                $(this).addClass(''); 
            } else if ( $(this).attr('id') == 'copy' ){
                $(this).addClass(''); 
            } else if ( $(this).attr('id') == 'noColor' ){
                $(this).addClass(''); 
            } else if ( $(this).attr('id') == 'goBackHistory' ){
                $(this).addClass('');
            }
            else {
                $(this).siblings('li').removeClass('active');
                $(this).addClass('active');   
                if ( graphic != 'brush'){
                    $("#setDimensionOfBrush").css("display", "none");
                }        
                if ( graphic != 'pencil'){
                    $("#setDimensionOfPencil").css("display", "none");
                }      
                if ( graphic != 'text'){
                    $("#editText").css("display", "none");
                }
                
            }    
        }
    });
}


function goBackInHistory(){

    var i = RepaintHistory.length - 1;
    if ( i > 0 ){
        RepaintHistory.splice(RepaintHistory.length - 1,1);
        invalidate();
        GraphicElements = new Array();
        GraphicElements = copyGraphicElements(RepaintHistory[RepaintHistory.length-1]);
        graphic = undefined;
        selectedItem = undefined;
        invalidate();
    } else {
        return; 
    }
    
}

function copyObjElement(obj){
    var copy;
    var copyType = obj.type;
    switch(copyType){
        case 'rect':
            copy = new Rect();
            copy.setX(obj.element.getX());
            copy.setY(obj.element.getY());
            copy.setWidth(obj.element.getWidth());
            copy.setHeight(obj.element.getHeight());
            copy.setBorder(obj.element.getBorder());
            copy.setFill(obj.element.getFill());
            copy.setColor(obj.element.getColor());
            copy.setBorderColor(obj.element.getBorderColor());
            copy.setRotate(obj.element.getRotate());
            rects.push(copy);
        break;
        
        case 'ellipse':
            copy = new Ellipse();
            copy.setFirstX(obj.element.getFirstX());
            copy.setFirstY(obj.element.getFirstY());
            copy.setSecondX(obj.element.getSecondX());
            copy.setSecondY(obj.element.getSecondX());
            copy.setX(obj.element.getX());
            copy.setY(obj.element.getY());
            copy.setRadiusX(obj.element.getRadiusX());
            copy.setRadiusY(obj.element.getRadiusY());
            copy.setRotation(obj.element.getRotation());
            copy.setStartAngle(obj.element.getStartAngle());
            copy.setEndAngle(obj.element.getEndAngle());
            copy.setColor(obj.element.getColor());
            copy.setBorderColor(obj.element.getBorderColor());
            copy.setFill(obj.element.getFill());
            copy.setRotate(obj.element.getRotate());
            ellipses.push(copy);
        break;

        case 'circle':
            copy = new Circle();
            copy.setX(obj.element.getX());
            copy.setY(obj.element.getY());
            copy.setFirstX(obj.element.getFirstX());
            copy.setFirstY(obj.element.getFirstY());
            copy.setSecondX(obj.element.getSecondX());
            copy.setSecondY(obj.element.getSecondY());
            copy.setRadius(obj.element.getRadius());
            copy.setStartAngle(obj.element.getStartAngle());
            copy.setColor(obj.element.getColor());
            copy.setBorder(obj.element.getBorder());
            copy.setBorderColor(obj.element.getBorderColor());
            copy.setFill(obj.element.getFill());
            circles.push(copy);
        break;

        case 'line':
            copy = new Line();
            copy.setStartX(obj.element.getStartX());
            copy.setStartY(obj.element.getStartY());
            copy.setCX(obj.element.getCX());
            copy.setCY(obj.element.getCY());
            copy.setEndX(obj.element.getEndX());
            copy.setEndY(obj.element.getEndY());
            copy.setColor(obj.element.getColor());
            lines.push(copy);
        break;

        case 'star':
            copy = new Star();
            copy.setX(obj.element.getX());
            copy.setY(obj.element.getY());
            copy.setWidth(obj.element.getWidth());
            copy.setHeight(obj.element.getHeight());
            copy.setCX(obj.element.getCX());
            copy.setCY(obj.element.getCY());
            copy.setSpikes(obj.element.getSpikes());
            copy.setOuterRadius(obj.element.getOuterRadius());
            copy.setInnerRadius(obj.element.getInnerRadius());
            copy.setBorder(obj.element.getBorder());
            copy.setFill(obj.element.getFill());
            copy.setColor(obj.element.getColor());
            copy.setBorderColor(obj.element.getBorderColor());
            copy.setRotate(obj.element.getRotate());
            copy.setXContains(obj.element.getXContains()+5);
            copy.setYContains(obj.element.getYContains()+5);
            copy.setWidthContains(obj.element.getWidthContains()+5);
            copy.setHeightContains(obj.element.getHeightContains()+5);
            stars.push(copy);
        break;

        case 'text':
            copy = new Text();
            copy.setText(obj.element.getText());
            copy.setX(obj.element.getX());
            copy.setY(obj.element.getY());
            copy.setColor(obj.element.getColor());
            copy.setBorder(obj.element.getBorder());
            copy.setRotate(obj.element.getRotate());
            copy.setBold(obj.element.getBold());
            copy.setItalic(obj.element.getItalic());
            copy.setUnderline(obj.element.getUnderline());
            copy.setmaxWidth(obj.element.getmaxWidth());
            copy.setfontSize(obj.element.getfontSize());
            copy.setFontFamily(obj.element.getFontFamily());
            copy.setFontStyle(obj.element.getFontSize(), obj.element.getFontFamily());
            copy.setWidth(obj.element.getWidth());
            copy.setHeight(obj.element.getHeight());
            texts.push(copy);
        break;

        case 'pencil':
            copy = new Pencil();
            copy.setStartX(obj.element.getStartX());
            copy.setStartY(obj.element.getStartY());
            copy.setCX(obj.element.getCX());
            copy.setCY(obj.element.getCY());
            copy.setEndX(obj.element.getEndX());
            copy.setEndY(obj.element.getEndY());
            copy.setColor(obj.element.getColor());
            copy.setWidth(obj.element.getWidth());
            copy.setPositions(obj.element.getPositions());
            pencils.push(copy);
        break;

    };
    return copy;
}

function copyGraphicElements(GE){
    rects=[];
    ellipses=[];
    circles=[];
    lines=[];
    stars=[];
    texts=[];
    if ( GE.length > 0 ){
        var copyGE=[];
        var i = 0;
        while(i < GE.length){
            
            var copyType = GE[i].type;
            var copyElement = copyObjElement(GE[i]);
            copyGE.push({'type': copyType, 'element': copyElement});
            switch(copyType){
                case 'rect':
                    rects.push(copyElement);
                break;
                case 'star':
                    stars.push(copyElement);
                break;
                case 'ellipse':
                    ellipses.push(copyElement);
                break;
                case 'circle':
                    circles.push(copyElement);
                case 'lines':
                    lines.push(copyElement);
                break;
                case 'text':
                    texts.push(copyElement);
                break;                    
            }
            i++;
        }

        //invalidate();        
        return copyGE;
    }
}

