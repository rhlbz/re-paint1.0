var Colors = [
    //black
    "rgb(0, 0, 0)",
    //gray 50%
    "rgb(127, 127, 127)",
    //rosso scuro
    "rgb(136, 0, 21)",
    //rosso
    "rgb(237, 28, 36)",
    //arancione
    "rgb(255, 127, 39)",
    //giallo
    "rgb(255, 242, 0)",
    //verde
    "rgb(34, 177, 76)",
    //turchese
    "rgb(0, 162, 232)",
    //indaco
    "rgb(63, 72, 204)",
    //viola
    "rgb(163, 73, 164)",
    //bianco
    "rgb(255, 255, 255)",
    //grigio 25%
    "rgb(195, 195, 195)",
    //marrone
    "rgb(185, 122, 87)",
    //rosa
    "rgb(255, 174, 201)",
    //dorato
    "rgb(255, 201, 14)",
    //giallo chiaro
    "rgb(239, 228, 176)",
    //verde limone
    "rgb(181, 230, 29)",
    //turchese chiaro
    "rgb(153, 217, 234)",
    //grigio blu
    "rgb(112, 146, 190)",
    //lavanda
    "rgb(200, 191, 231)",
    //
    "rgb(245, 245, 245)",
    //
    "rgb(230,230,230)",
    //
    "rgb(188, 135, 135)",                
    //
    "rgb(255, 204, 204)",
    //
    "rgb(255, 229, 204)",
    //
    "rgb(255, 255, 204)",
    //
    "rgb(229, 255, 204)",
    //
    "rgb(204,255,255)",
    //
    "rgb(204, 229, 255)",
    //
    "rgb(229, 204, 255)"                
];