////////////////////// PALETTE DEI COLORI

function onMouseDownPalette(){
    if ( selectedIndexColor != undefined ){
        colors[selectedIndexColor].setBorder(false);        
    }

    for ( var i = 0; i < colors.length; i++ ){
        if ( colors[i].Contains(event.offsetX, event.offsetY) ){
            selectedIndexColor = i;
            colors[selectedIndexColor].setBorder(true);
            invalidatePaletteColors();
        } 
    }
}

function drawRectPalette(palette , color){   
    if ( color.border ) {
        palette.beginPath();
        palette.lineWidth = 1.5;
        palette.fillStyle = "rgb(205, 214, 229)";
        palette.fillRect(color.getX() - 1, color.getY() - 1 , color.getWidth() + 2, color.getHeight() + 2 );
        palette.fillStyle = color.getColor();
        palette.rect(color.getX() - 2, color.getY() - 2 , color.getWidth() + 4, color.getHeight() + 4 );
        palette.fillRect(color.getX() - 2, color.getY() - 2 , color.getWidth() + 4, color.getHeight() + 4 );
        palette.stroke();
        palette.closePath();
    } else {        
        palette.fillStyle ="rgb(205, 214, 229)";
        palette.fillRect(color.getX() - 1, color.getY() - 1 , color.getWidth() + 2, color.getHeight() + 2 );
        palette.fillStyle = color.getColor();
        palette.rect(color.getX(), color.getY(), color.getWidth(), color.getHeight());
        palette.fillRect(color.getX(), color.getY(), color.getWidth(), color.getHeight());
        palette.closePath();
    }
}

function invalidatePaletteColors(){
    palette.clearRect(palette.canvas.clientTop, palette.canvas.clientLeft, palette.canvas.clientWidth, palette.canvas.clientHeight);
    for ( var i = 0; i < colors.length; i++ ){
        drawRectPalette(palette, colors[i]);
    }
}
