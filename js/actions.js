/////////////// ACTIONS SUL CANVAS

function onMouseDown(){

    if ( selectedItem != undefined ){
        indexBackgroundColor = indexBackgroundColor; 
    } else {
        if ( setFill ){
            $("#fill").addClass("active");
            indexBackgroundColor = selectedIndexColor;
            console.log(indexBackgroundColor);
            invalidate();
            $("#fill").addClass('active');
        }
    }
    onCheckRect();
    onCheckEllipse();
    onCheckLine();
    onCheckCircle();
    onCheckText();
    onCheckStar();
    invalidate();
    
    console.log(graphic)

    if ( graphic != 'brush') {
        $("#setDimensionOfBrush").css("display", "none");
    }
    if ( graphic != 'pencil') {
        $("#setDimensionOfPencil").css("display", "none");
    }
    if ( graphic != 'text' ){
        $("#setupText").css("display", "none");
    }
    cicleHookTop = new Circle();
    cicleHookBottom = new Circle();

    setActiveButton();

    switch(graphic) {

        case 'rect':
            onRectDown();
        break;

        case 'ellipse':
            onEllipseDown();
        break; 

        case 'line':
            onLineDown();
        break;

        case 'circle':
            onCircleDown();
        break;

        case 'star':
            onStarDown();
        break;

        case 'brush':
            onPencilDown();
        break;

        case 'pencil':
            onPencilDown();
        break;

        case 'text':
            onTextDown();
        break;

        case undefined:
            onSelect();
        break;

    }
}

function onMouseMove(){

    if ( setFill ){
        document.body.style.cursor = "src('img/fillPaint.png', auto)";
        //document.body.style.cursor = 'alias';
    } else {
        document.body.style.cursor = 'default';
    }

    switch(graphic) {

        case 'rect':
            onRectMove();   
        break;

        case 'ellipse':
            onEllipseMove();
        break;   

        case 'line':
            onLineMove();
        break; 

        case 'circle':
            onCircleMove();
        break;   

        case 'star':
            onStarMove();
        break;   

        case 'brush':
        onPencilMove();
        break;

        case 'pencil':
            onPencilMove();
        break;

        case 'bezier':
            onBezierMove();
        break; 

        case 'quadraticCurve':
            onQuadraticCurveMove();
        break; 

        case 'text':
            onTextMove();
        break; 
    }
}

function onMouseUp(){
    switch(graphic) {

        case 'rect':
            onRectUp();   
        break;

        case 'ellipse':
            onEllipseUp();
        break;  

        case 'line':
            onLineUp();
        break;

        case 'circle':
            onCircleUp();    
        break;

        case 'star':
            onStarUp();    
        break;

        case 'brush':
        onPencilUp();
        break;

        case 'pencil':
            onPencilUp();
        break;

        case 'bezier':
            onBezierUp();
        break;

        case 'quadraticCurve':
            onQuadraticCurveUp();
        break;

        case 'text':
            onTextUp();
        break;

    }   
}