
function onKeyPress(){
    
    var keyCode = event.code;
    var keyProperties = event;
    console.log(event);
    if ( event.key == 'Shift' ){
        regular = true;
    } else {
        regular = false;
    }

    switch (keyCode){

        case 'ArrowRight': 
            if ( selectedItem != undefined ){
                var type = GraphicElements[selectedItem].type;
                var selection = copyObjElement(GraphicElements[selectedItem]);
                switch(type) {
                    case 'rect':
                        selection.setX(selection.getX()+3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'rect', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'rect', 'element': selection});
                    break;
                    case 'circle':
                        selection.setX(selection.getX()+3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'circle', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem, 1, {'type': 'circle', 'element': selection});
                    break;
                    case 'ellipse':
                        selection.setX(selection.getX()+3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'ellipse', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'ellipse', 'element': selection});
                    break;
                    case 'star':
                        selection.setCX(selection.getCX()+3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'star', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'star', 'element': selection});
                    break;
                    case 'line':
                        selection.setStartX(selection.getStartX()+3);
                        selection.setEndX(selection.getEndX()+3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'line', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'line', 'element': selection});
                    break;
                }
            }
            invalidate();
        break;
        case 'ArrowLeft': 
            if ( selectedItem != undefined ){
                var type = GraphicElements[selectedItem].type;
                var selection = copyObjElement(GraphicElements[selectedItem]);
                switch(type) {
                    case 'rect':
                        selection.setX(selection.getX()-3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'rect', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'rect', 'element': selection});
                    break;
                    case 'circle':
                        selection.setX(selection.getX()-3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'circle', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'circle', 'element': selection});
                    break;
                    case 'ellipse':
                        selection.setX(selection.getX()-3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'ellipse', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'ellipse', 'element': selection});
                    break;
                    case 'star':
                        selection.setCX(selection.getCX()-3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'star', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'star', 'element': selection});
                    break;
                    case 'line':
                        selection.setStartX(selection.getStartX()-3);
                        selection.setEndX(selection.getEndX()-3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'line', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'line', 'element': selection});
                    break;
                }
            }
            invalidate();
        break;
        case 'ArrowDown': 
            if ( selectedItem != undefined ){
                var type = GraphicElements[selectedItem].type;
                var selection = copyObjElement(GraphicElements[selectedItem]);
                switch(type) {
                    case 'rect':
                        selection.setY(selection.getY()+3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'rect', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'rect', 'element': selection});
                    break;
                    case 'circle':
                        selection.setY(selection.getY()+3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'circle', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'circle', 'element': selection});
                    break;
                    case 'ellipse':
                        selection.setY(selection.getY()+3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'ellipse', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'ellipse', 'element': selection});
                    break;
                    case 'star':
                        selection.setCY(selection.getCY()+3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'star', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'star', 'element': selection});
                    break;
                    case 'line':
                        selection.setStartY(selection.getStartY()+3);
                        selection.setEndY(selection.getEndY()+3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'line', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'line', 'element': selection});
                    break;
                }
            }
            invalidate();
        break;
        case 'ArrowUp': 
            if ( selectedItem != undefined ){
                var type = GraphicElements[selectedItem].type;
                var selection = copyObjElement(GraphicElements[selectedItem]);
                switch(type) {
                    case 'rect':
                        selection.setY(selection.getY()-3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'rect', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'rect', 'element': selection});
                    break;
                    case 'circle':
                        selection.setY(selection.getY()-3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'circle', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'circle', 'element': selection});
                    break;
                    case 'ellipse':
                        selection.setY(selection.getY()-3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'ellipse', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'ellipse', 'element': selection});
                    break;
                    case 'star':
                        selection.setCY(selection.getCY()-3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'star', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'star', 'element': selection});
                    break;
                    case 'line':
                        selection.setStartY(selection.getStartY()-3);
                        selection.setEndY(selection.getEndY()-3);
                        var tmp = copyGraphicElements(GraphicElements);
                        tmp.splice(selectedItem, 1, {'type': 'line', 'element': selection});
                        RepaintHistory.push(tmp);
                        GraphicElements.splice(selectedItem,1, {'type': 'line', 'element': selection});
                    break;
                }
            }
            invalidate();
        break;
        //RIMOZIONE ELEMENTO SELEZIONATO
        //CLICK SU ELEMENTO + CANC
        case 'Delete':
            removeSelectedGraphicElement();
        break;
        //RIMOZIONE ULTIMO PENCIL o BRUSH
        //CTRL + ALT + P
        case 'KeyP':
            if ( keyProperties.ctrlKey && keyProperties.altKey && keyProperties.composed){
                
                removeLastElementOfParticulatType('pencil');
            }
        break;
        //RIMOZIONE ULTIMO RECT
        //CTRL + ALT + R
        case 'KeyR':
            if ( keyProperties.ctrlKey && keyProperties.altKey && keyProperties.composed){
                removeLastElementOfParticulatType('rect');
            }
        break;
        //RIMOZIONE ULTIMA ELLISSE
        //CTRL + ALT + E
        case 'KeyE':
            if ( keyProperties.ctrlKey && keyProperties.altKey && keyProperties.composed){
                removeLastElementOfParticulatType('ellipse');
            }
        break;
        //RIMOZIONE ULTIMO CERCHIO
        //CTRL + ALT + O
        case 'KeyO':
            if ( keyProperties.ctrlKey && keyProperties.altKey && keyProperties.composed){
                removeLastElementOfParticulatType('circle');
            }
        break;
        //RIMOZIONE ULTIMA LINEA
        //CTRL + ALT + L
        case 'KeyL':
            if ( keyProperties.ctrlKey && keyProperties.altKey && keyProperties.composed){
                removeLastElementOfParticulatType('line');
            }
        break;
        //RIMOZIONE ULTIMA STELLA
        //CTRL + ALT + S
        case 'KeyS':
            if ( keyProperties.ctrlKey && keyProperties.altKey && keyProperties.composed){
                removeLastElementOfParticulatType('star');
            }
        break;
        //RIMOZIONE ULTIMA STELLA
        //CTRL + ALT + S
        case 'KeyT':
            if ( keyProperties.ctrlKey && keyProperties.altKey && keyProperties.composed){
                removeLastElementOfParticulatType('text');
            }
        break;
        //ANNULLA
        //CTRL + ALT + Z
        case 'KeyZ':
            if ( keyProperties.ctrlKey && keyProperties.altKey && keyProperties.composed){
                goBackInHistory();
            }
        break;
        case 'KeyC':
            if ( keyProperties.ctrlKey ){
                copySelectedItem();
            }
        break;
        case 'KeyV':
            if ( keyProperties.ctrlKey ){
                pasteSelectedItem();
            }
        break;

    }
    

}

