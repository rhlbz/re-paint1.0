/////////////////////////////////////   CERCHIO     /////////////////////////////////////////////////////////////////////

function onPencilDown(){

    dragPencil = true;
    pencil = new Pencil();
    if ( graphic == 'pencil' ){
        var dimensionPencil = $("#setDimensionOfPencil option:selected").val();
    } else {
        var dimensionPencil = $("#setDimensionOfBrush option:selected").val();
    }
    
    pencil.setWidth(dimensionPencil);

    pencil.setStartX(event.offsetX);
    pencil.setStartY(event.offsetY);
    if( selectedIndexColor != undefined ){
        pencil.setColor(colors[selectedIndexColor].getColor());
    }
}


function onPencilMove(){
    if ( dragPencil ){
        invalidate();
        var pencilPositions = pencil.getPositions();
        var pos = [event.offsetX, event.offsetY];
        pencil.setPositions( pencilPositions.concat([pos]) );
        if ( selectedIndexColor != undefined ){
            pencil.setColor(colors[selectedIndexColor].getColor());
        }
        drawPencil(ctx, pencil);
    }

}

function onPencilUp(){
    
    dragPencil = false;
    isinPencil = false;
    if ( !dragPencil) {
        GraphicElements.push({'type': 'pencil', 'element': pencil});
        var tmp = copyGraphicElements(GraphicElements);
        //tmp.push({'type' : 'rect', 'element': rect});
        RepaintHistory.push(tmp);
        tmp = undefined;
       
    } else {
        pencilPositions = undefined;
        pos = [];
    }
    pencil = new Pencil();

}

function drawPencil(ctx, pencil){

    ctx.beginPath();  
    ctx.setLineDash([]);  
    ctx.lineWidth = pencil.getWidth();
    ctx.strokeStyle = pencil.getColor();
    ctx.moveTo(pencil.getStartX(), pencil.getStartY());
    for (var p = 0 ; p < pencil.getPositions().length; p++){
        ctx.lineTo(pencil.getPositions()[p][0], pencil.getPositions()[p][1]);  
    }  
    
    ctx.stroke();
    ctx.closePath();

}