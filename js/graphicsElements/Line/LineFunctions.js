///////////////////////////////////////////   LINEA   /////////////////////////////////////////////////////////////////////
var selection = undefined;

function onLineDown(){
    NotContainsLine=true;
    if (lines.length > 0){
        dragLine = false;
        var i = 0; 
        while(NotContainsLine && i < GraphicElements.length){
            if (GraphicElements[i].type == 'line' ){
                if ( GraphicElements[i].element.Contains(event.offsetX, event.offsetY) || (GraphicElements[i].element.Contains(event.offsetX - 5, event.offsetY-5)) || (GraphicElements[i].element.Contains(event.offsetX+5, event.offsetY+5)) ){
                    selectedItem = i;
                    isinLine = true;

                    selection = copyObjElement(GraphicElements[i]);
                    RepaintHistory[RepaintHistory.length-1].splice(selectedItem, 1, {'type': 'line', 'element': selection});

                    offsetX = event.offsetX - selection.getCX();
                    offsetY = event.offsetX - selection.getCY();
                    invalidate();
                    drawSelectionBoxLine(ctx, selection);
                    line = new Line();
                    xClickD=event.offsetX;
                    yClickD=event.offsetY;
                    xa=selection.getStartX();
                    ya=selection.getStartY();
                    xb=selection.getEndX();
                    yb=selection.getEndY();
                    NotContainsLine=false;
                } else {
                    i++;
                }
            } else {
                i++
            }
        }
        if(NotContainsLine){
            selectedItem=undefined;
            invalidate();
        }
    }

    if ( selectedItem != undefined ){   
        circleHookTop.setRadius(5);
        circleHookTop.setFill(true);
        circleHookTop.setX(selection.getStartX());
        circleHookTop.setY(selection.getStartY());
        circleHookTop.setColor('red');

                    
        circleHookBottom.setRadius(5);
        circleHookBottom.setFill(true);
        circleHookBottom.setX(selection.getEndX());
        circleHookBottom.setY(selection.getEndY());
        circleHookBottom.setColor('red');

        if ( setFill ){
            selection.setColor(colors[selectedIndexColor].getColor());
        } 

        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);

        if ( circleHookTop.Contains(event.offsetX, event.offsetY) ){
            transformLineTop = true;    
            document.body.style.cursor = "se-resize";
            console.log("click su hook top ");
        } else {
            transformLineTop = false;
            document.body.style.cursor = "default";
        }
        if ( circleHookBottom.Contains(event.offsetX, event.offsetY) ) {
            transformLineBottom = true;
            document.body.style.cursor = "se-resize";
        }   else {
            transformLineBottom = false;
            document.body.style.cursor = "default";
        }
    } else {
        dragLine = true;
        line = new Line();
        line.setStartX(event.offsetX);
        line.setStartY(event.offsetY);
        line.setEndX(0);
        line.setEndY(0);
        if ( selectedIndexColor != undefined ){
            line.setColor(colors[selectedIndexColor].getColor());
        }
    }

}

function onLineMove(){
    if (dragLine && !isinLine && !transformLineTop && !transformLineBottom){
        invalidate();

        line.setEndX(event.offsetX);
        line.setEndY(event.offsetY);


        drawLine(ctx, line);
        drawSelectionBoxLine(ctx, line);
        
    } 
    
    if ( isinLine && !transformLineTop && !transformLineBottom ){
        invalidate();
        dX=event.offsetX-xClickD;
        dY=event.offsetY-yClickD;
        
        selection.setStartX(xa+dX);
        selection.setStartY(ya+dY);
        selection.setEndX(xb+dX);
        selection.setEndY(yb+dY);

        circleHookTop.setX(selection.getStartX());
        circleHookTop.setY(selection.getStartY());
        circleHookBottom.setX(selection.getEndX());
        circleHookBottom.setY(selection.getEndY());

        drawLine(ctx, selection);
        drawSelectionBoxLine(ctx, selection);
        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);
    }

    if ( isinLine && transformLineTop ){

        invalidate();

        selection.setStartX(event.offsetX);
        selection.setStartY(event.offsetY);

        circleHookTop.setX(selection.getStartX());
        circleHookTop.setY(selection.getStartY());

        drawLine(ctx, selection);
        drawSelectionBoxLine(ctx, selection);
        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);
    }

    if ( isinLine && transformLineBottom ){

        invalidate();

        selection.setEndX( event.offsetX );
        selection.setEndY( event.offsetY );

        drawLine(ctx, selection);

        circleHookBottom.setX(event.offsetX);
        circleHookBottom.setY(event.offsetY);

        drawSelectionBoxLine(ctx, selection);
        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);

    }

}

function onLineUp(){
    dragLine = false;
    isinLine = false;
    transformLineTop = false;
    transformLineBottom = false;

    if ( (!isinLine) && (line.getEndX() > 0) && (line.getEndY() > 0) ){

        lines.push(line);
        
    }

    if (selectedItem != undefined ){
        var tmp = copyGraphicElements(GraphicElements);
        tmp.splice(selectedItem, 1, {'type': 'line', 'element': selection});
        RepaintHistory.push(tmp);
        GraphicElements.splice(selectedItem,1, {'type': 'line', 'element': selection});
        tmp = undefined;
        selection = undefined;
    
    } else {
        GraphicElements.push({'type' : 'line', 'element': line});
        //RepaintHistory.push(GraphicElements);
        var tmp = copyGraphicElements(GraphicElements);
        //tmp.push({'type' : 'rect', 'element': rect});
        RepaintHistory.push(tmp);
        tmp = undefined;
        line = undefined;
        
    }
    

}

function drawLine(ctx, line){
    ctx.beginPath();  
    ctx.lineWidth = 1;   
    ctx.setLineDash([]);  
    ctx.strokeStyle = line.getColor();
    ctx.moveTo(line.getStartX(), line.getStartY());    
    ctx.lineTo(line.getEndX(), line.getEndY());  
    ctx.stroke();
    ctx.closePath();
}

function drawSelectionBoxLine(ctx, line) { 
    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.setLineDash([1, 3]);
    ctx.strokeStyle = 'gray';
    ctx.rect(line.getStartX(), line.getStartY(), (line.getEndX()-line.getStartX() ), (line.getEndY() - line.getStartY()) );
    ctx.stroke();   
    ctx.closePath();
}