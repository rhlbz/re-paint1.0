class Line {
    
    constructor(){
        this.startX; //posizione X in cui si vuol far iniziare la linea
        this.startY; //posizione Y in cui si vuol far iniziare la linea
        this.cx;
        this.cy;
        this.endX; //posizione X in cui si vuol far finire la linea
        this.endY; //posizione Y in cui si vuol far finire la linea
        this.color = 'black';
    }

    //METHODS
    Contains(mouseX, mouseY){ 
        if ( this.startX == mouseX || this.startY == mouseY ){
            return true;
        } else if(this.startY>this.endY){
            if((mouseY<this.startY)&&(mouseY>this.endY)){
                var epsilon=(-mouseX*this.endY+this.startX*this.endY+mouseX*this.startY-this.startX*this.startY+mouseY*this.endX-this.startY*this.endX-mouseY*this.startX+this.startX*this.startY)*(1/(this.endY-this.startY-this.endX+this.startX));
                if (Math.abs(epsilon)<100){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            if((mouseY>this.startY)&&(mouseY<this.endY)){
                var epsilon=(-mouseX*this.endY+this.startX*this.endY+mouseX*this.startY-this.startX*this.startY+mouseY*this.endX-this.startY*this.endX-mouseY*this.startX+this.startX*this.startY)*(1/(this.endY-this.startY-this.endX+this.startX));
                if (Math.abs(epsilon)<100){
                    return true;
                }else{
                    return false;
                }
            } else{
                return false;
            }
        }
    }

    //GET & SET
    getStartX(){
        return this.startX;
    }
    setStartX(startX){
        this.startX = startX;
    }

    getStartY(){
        return this.startY;
    }
    setStartY(startY){
        this.startY = startY;
    }

    getCX(){
        return this.cx;
    }
    setCX(cx){
        this.cx = cx;
    }

    getCY(){
        return this.cy;
    }
    setCY(cy){
        this.cy = cy;
    }
    

    getEndX(){
        return this.endX;
    }
    setEndX(endX){
        this.endX = endX;
    }

    getEndY(){
        return this.endY;
    }
    setEndY(endY){
        this.endY = endY;
    }

    getColor(){
        return this.color;
    }
    setColor(color){
        this.color = color;
    }

}