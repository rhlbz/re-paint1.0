class Star {

    constructor() {
        this.x;
        this.y;
        this.width;
        this.height;
        this.cx = 0;
        this.cy = 0;
        this.spikes = 5;
        this.outerRadius = 7.5;
        this.innerRadius = 3.75;
        this.border = true;
        this.fill = false;
        this.color;
        this.borderColor = 'black';
        this.rotate = 0;
        this.xContains = 0;
        this.yContains;
        this.widthContains;
        this.heightContains;
    }

    Contains(mouseX, mouseY){
        if((this.widthContains>0)&&(this.heightContains>0)){ 
            if ( (mouseX > Math.abs(this.xContains)) && (mouseX < (this.widthContains + Math.abs(this.xContains) )) && (mouseY > Math.abs(this.yContains)) && (mouseY < (this.heightContains + this.y)) ) {
                return true;
            } else {
                return false;
            }
        } else if ((this.widthContains>0)&&(this.heightContains<0)){ //verso destra e alto
            if ( (mouseX > Math.abs(this.xContains)) && (mouseX < (this.widthContains + Math.abs(this.xContains) )) && (mouseY < Math.abs(this.yContains)) && (mouseY > (this.heightContains + Math.abs(this.yContains))) ) {
                return true;
            } else {
                return false;
            }
        }else if ((this.widthContains<0)&&(this.heightContains>0)){ //verso sinistra e basso
            if ( (mouseX < Math.abs(this.xContains)) && (mouseX > (this.widthContains + Math.abs(this.xContains) )) && (mouseY > Math.abs(this.yContains)) && (mouseY < (this.heightContains + Math.abs(this.yContains))) ) {
                return true;
            } else {
                return false;
            }
        }else if ((this.widthContains<0)&&(this.heightContains<0)){ //verso sinistra e alto
            if ( (mouseX < Math.abs(this.xContains)) && (mouseX > (this.widthContains + Math.abs(this.xContains) )) && (mouseY < Math.abs(this.yContains)) && (mouseY > (this.heightContains + Math.abs(this.yContains))) ) {
                return true;
            } else {
                return false;
            }
        }    
    }

    getX(){
        return this.x;
    }
    setX(x){
        this.x = x;
    }

    getY(){
        return this.y;
    }
    setY(y){
        this.y = y;
    }

    getWidth(){
        return this.width;
    }
    setWidth(width){
        this.width = width;
    }

    getHeight(){
        return this.height;
    }
    setHeight(height){
        this.height = height;
    }

    getCX(){
        return this.cx;
    }
    setCX(cx){
        this.cx = cx;
    }

    getCY(){
        return this.cy;
    }
    setCY(cy){
        this.cy = cy;
    }

    getSpikes(){
        return this.spikes;
    }
    setSpikes(spikes){
        this.spikes = spikes;
    }

    getOuterRadius(){
        return this.outerRadius;
    }
    setOuterRadius(outerRadius){
        this.outerRadius = outerRadius;
    }

    getInnerRadius(){
        return this.innerRadius;
    }
    setInnerRadius(innerRadius){
        this.innerRadius = innerRadius;
    }

    getBorder(){
        return this.border;
    }
    setBorder(border){
        this.border = border;
    }

    getFill(){
        return this.fill;
    }
    setFill(fill){
        this.fill = fill;
    }
    
    getColor(){
        return this.color;
    }
    setColor(color){
        this.color = color;
    }

    getBorderColor(){
        return this.borderColor;
    }
    setBorderColor(borderColor){
        this.borderColor = borderColor;
    }
    
    getRotate(){
        return this.rotate;
    }
    setRotate(rotate){
        this.rotate = rotate;
    }

    getXContains(){
        return this.xContains;
    } 
    setXContains(xContains){
        this.xContains = xContains;
    }

    getYContains(){
        return this.yContains;
    }
    setYContains(yContains){
        this.yContains = yContains;
    }

    getWidthContains(){
        return this.widthContains;
    }
    setWidthContains(widthContains){
        this.widthContains = widthContains;
    }

    getHeightContains(){
        return this.heightContains;
    }
    setHeightContains(heightContains){
        this.heightContains = heightContains;
    }

}