var tempX;
var tempY;
var selection = undefined;

function onStarDown(){ 
    NotContainsStar = true;
    if ( GraphicElements.length > 0 ){
        dragStar = false;
        var i = 0;
        while(NotContainsStar && i < GraphicElements.length){
            if( GraphicElements[i].type == 'star' ){
                if ( (GraphicElements[i].element.Contains(event.offsetX, event.offsetY)) || (GraphicElements[i].element.Contains(event.offsetX - 5, event.offsetY-5)) || (GraphicElements[i].element.Contains(event.offsetX+5, event.offsetY+5) )){
                    selectedItem = i;
                    isinStar = true;
                    selection = copyObjElement(GraphicElements[i]);
                    RepaintHistory[RepaintHistory.length-1].splice(selectedItem, 1, {'type': 'star', 'element': selection});
                    offsetX = event.offsetX - selection.getCX();
                    offsetY = event.offsetY - selection.getCY();
                    star = new Star();
                    if ( setFill ){
                       selection.setFill(setFill);
                        if ( selectedIndexColor != undefined ){
                           selection.setColor(colors[selectedIndexColor].getColor());
                           selection.setBorderColor(colors[selectedIndexColor].getColor());
                            drawSelectionBoxStar(ctx,selection);
                        } else {
                           selection.setColor('black');
                           selection.setBorderColor('black');
                           drawSelectionBoxStar(ctx,selection);
                        }
                    }

                    drawSelectionBoxStar(ctx,selection);

                    if ( circleHookTop.Contains(event.offsetX, event.offsetY) ){
                        transformStarTop = true;
                    }
                    
                    if (circleHookBottom.Contains(event.offsetX, event.offsetY)) {
                        transformStarBottom = true;
                    }


                    NotContainsStar = false;
                } else {
                    i++;
                }
            } else {
                i++;
            }
        }
        if ( NotContainsStar ){
            selectedItem = undefined;
        }
    }

    if (selectedItem != undefined ){
        
        circleHookTop.setRadius(5);
        circleHookTop.setFill(true);
        circleHookTop.setColor('red');
        circleHookTop.setX(selection.getXContains());
        circleHookTop.setY(selection.getYContains());

        circleHookBottom.setRadius(5);
        circleHookBottom.setFill(true);
        circleHookBottom.setColor('red');
        circleHookBottom.setX(selection.getXContains() +selection.getWidthContains());
        circleHookBottom.setY(selection.getYContains() +selection.getHeightContains());

        if ( setFill ){
           selection.setFill(true);
           selection.setColor(colors[selectedIndexColor].getColor());
            invalidate();
            drawSelectionBoxStar(ctx,selection);
        }

        if ( selectedIndexColor != undefined ){
           selection.setBorderColor(colors[selectedIndexColor].getColor());
            invalidate();
            drawSelectionBoxStar(ctx,selection);
        } 

        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);

        if ( circleHookTop.Contains(event.offsetX, event.offsetY) ){
            transformStarTop = true;    
            document.body.style.cursor = "se-resize";
        } else {
            transformStarTop = false;
            document.body.style.cursor = "default";
        }
        if ( circleHookBottom.Contains(event.offsetX, event.offsetY) ) {
            transformStarBottom = true;
            document.body.style.cursor = "se-resize";
        }   else {
            transformStarBottom = false;
            document.body.style.cursor = "default";
        }

    } else {
        dragStar = true;
        star = new Star();
        ctx.restore();
        star.setX(event.offsetX);
        star.setY(event.offsetY);
        tempX = star.getX();
        tempY = star.getY();

        if ( setFill ){
            star.setFill(setFill);
            if ( selectedIndexColor != undefined ){
                star.setColor(colors[selectedIndexColor].getColor());
            } else {
                star.setColor('black');
            }
        } 
        
        star.setWidth(0);
        star.setHeight(0);
    }

}

function onStarMove(){
    if ( dragStar && !isinStar && !transformStarTop && !transformStarBottom ){
        invalidate();

        star.setWidth(event.offsetX - star.getX());
        star.setHeight(event.offsetY - star.getY());

        if ( tempX < event.offsetX || tempY < event.offsetY ){
            tempX = event.offsetX;
            tempY = event.offsetY;
            star.setOuterRadius(star.getOuterRadius()+5);
            if ( star.getOuterRadius() % 10){
                star.setInnerRadius(star.getInnerRadius() + 2.5);
            }
        } else if ( tempX > event.offsetX || tempY > event.offsetY ){
            tempX = event.offsetX;
            tempY = event.offsetY;
            star.setOuterRadius(star.getOuterRadius()- 5);
            if ( star.getOuterRadius() % 10){
                star.setInnerRadius(star.getInnerRadius() - 2.5);
            }
        } 
        
        star.setCX(star.getX() + star.getWidth());
        star.setCY(star.getY() + star.getHeight());
        
        if ( star.getWidth() < 0 && star.getHeight() < 0 ) {
            return ;
        }

        drawStar(ctx, star);
        drawSelectionBoxStar(ctx, star);
    }

    if ( isinStar  && !transformStarTop && !transformStarBottom ){

       selection.setCX( event.offsetX - offsetX);
       selection.setCY( event.offsetY - offsetY);
        invalidate();
        drawStar(ctx,selection);
        drawSelectionBoxStar(ctx,selection);
        invalidate();

        circleHookTop.setX(selection.getXContains());
        circleHookTop.setY(selection.getYContains());

        circleHookBottom.setX(selection.getXContains() +selection.getWidthContains());
        circleHookBottom.setY(selection.getYContains() +selection.getHeightContains());

        invalidate();
        drawSelectionBoxStar(ctx,selection);
        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);

    }

    if ( isinStar && transformStarTop ){
        invalidate();
        var diffX =selection.getXContains() - event.offsetX;
        var diffY =selection.getYContains() - event.offsetY;
        if ( ( event.offsetX >selection.getXContains() ) && (event.offsetY >selection.getYContains() ) )  {
           selection.setInnerRadius(selection.getInnerRadius() - 2.5);
           selection.setOuterRadius(selection.getOuterRadius() - 5);
        } else {
           selection.setInnerRadius(selection.getInnerRadius() + 2.5);
           selection.setOuterRadius(selection.getOuterRadius() + 5);
        }
        
        circleHookTop.setX(selection.getXContains() - diffX);
        circleHookTop.setY(selection.getYContains() - diffY);
        circleHookBottom.setX(selection.getXContains() +selection.getWidthContains());
        circleHookBottom.setY(selection.getYContains() +selection.getHeightContains());
        invalidate();
        drawSelectionBoxStar(ctx,selection);
        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);

    }
    
    if ( isinStar && transformStarBottom ){

        invalidate();
        if ( (event.offsetX < (selection.getXContains() +selection.getWidthContains() ))
        && (event.offsetY < (selection.getYContains() +selection.getHeightContains()) ) ){
           selection.setInnerRadius(selection.getInnerRadius() - 2.5);
           selection.setOuterRadius(selection.getOuterRadius() - 5);
        } else {
           selection.setInnerRadius(selection.getInnerRadius() + 2.5);
           selection.setOuterRadius(selection.getOuterRadius() + 5);
        }
        invalidate();
        circleHookTop.setX(selection.getXContains());
        circleHookTop.setY(selection.getYContains());

        circleHookBottom.setX(selection.getXContains() +selection.getWidthContains());
        circleHookBottom.setY(selection.getYContains() +selection.getHeightContains());

        invalidate();

        drawStar(ctx,selection);
        drawSelectionBoxStar(ctx,selection);
        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);
    }

}

function onStarUp(){
    dragStar = false;
    isinStar = false;
    if ( !isinStar && (star.getWidth() > 0) &&( star.getHeight() > 0)){
        stars.push(star);
        
    }
    if ( selectedItem != undefined ){
        var tmp = copyGraphicElements(GraphicElements);
        tmp.splice(selectedItem, 1, {'type': 'star', 'element': selection});
        RepaintHistory.push(tmp);
        GraphicElements.splice(selectedItem,1, {'type': 'star', 'element': selection});
        tmp = undefined;
        selection = undefined;

    } else {
        GraphicElements.push({'type' : 'star', 'element': star});
        //RepaintHistory.push(GraphicElements);
        var tmp = copyGraphicElements(GraphicElements);
        //tmp.push({'type' : 'rect', 'element': rect});
        RepaintHistory.push(tmp);
        tmp = undefined;
        star = undefined;
        
    }
    

}

function drawStar(ctx, star){
    ctx.save();
    var rotation = Math.PI / 2 * 3;
    var step = Math.PI / star.getSpikes();
    if ( star.getFill() ){
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.setLineDash([]);
        ctx.strokeStyle = star.getBorderColor();
        ctx.fillStyle = star.getColor();
        ctx.moveTo(x, y - star.getOuterRadius());
        for ( var i = 0; i < star.getSpikes(); i++ ){
            x = star.getCX() + Math.cos(rotation) * star.getOuterRadius();
            y = star.getCY() + Math.sin(rotation) * star.getOuterRadius();
            ctx.lineTo(x, y);
            rotation += step;
        
            x = star.getCX() + Math.cos(rotation) * star.getInnerRadius();
            y = star.getCY() + Math.sin(rotation) * star.getInnerRadius();
            ctx.lineTo(x, y);
            rotation += step;
        }
        ctx.lineTo(star.getCX(), star.getCY() - star.getOuterRadius());
        ctx.stroke();
        ctx.fill();
        ctx.restore();
        ctx.closePath();
    } else {
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.setLineDash([]);
        ctx.strokeStyle = star.getBorderColor();
        var x = star.getCX(); 
        var y = star.getCY();
        ctx.moveTo(x, y - star.getOuterRadius());
        for ( var i = 0; i < star.getSpikes(); i++ ){
            x = star.getCX() + Math.cos(rotation) * star.getOuterRadius();
            y = star.getCY() + Math.sin(rotation) * star.getOuterRadius();
            ctx.lineTo(x, y);
            rotation += step;
        
            x = star.getCX() + Math.cos(rotation) * star.getInnerRadius();
            y = star.getCY() + Math.sin(rotation) * star.getInnerRadius();
            ctx.lineTo(x, y);
            rotation += step;
        }
        ctx.lineTo(star.getCX(), star.getCY() - star.getOuterRadius());
        ctx.stroke();
        ctx.restore();
        ctx.closePath();
    }
    ctx.closePath();
    ctx.restore();
}

function drawSelectionBoxStar(ctx, star){
    var rotation = Math.PI / 2 * 3;
    ctx.beginPath();
    ctx.save();
    ctx.setLineDash([5, 15]);
    var maxX; 
    var minY;
    var minX;
    var maxY;
    var step = Math.PI / star.getSpikes();
    var xs=[];
    var ys=[];
    for ( var i = 0; i < star.getSpikes(); i++ ){
        x = star.getCX() + Math.cos(rotation) * star.getOuterRadius();
        y = star.getCY() + Math.sin(rotation) * star.getOuterRadius();
        xs.push(x);
        ys.push(y);
        rotation += step;

        x = star.getCX() + Math.cos(rotation) * star.getInnerRadius();
        y = star.getCY() + Math.sin(rotation) * star.getInnerRadius();
        rotation += step;
        }
    maxX = Math.max.apply(null, xs);
    minY = Math.min.apply(null, ys);
    minX = Math.min.apply(null, xs);
    maxY = Math.max.apply(null, ys);
    star.setXContains((minX));
    star.setYContains((minY));
    ctx.lineWidth = 1;
    ctx.strokeStyle = 'gray';
    ctx.rect(minX, minY, (maxX - minX), (maxY - minY));
    star.setWidthContains((maxX - minX));
    star.setHeightContains((maxY - minY));
    ctx.stroke();
    ctx.closePath();
    
}

