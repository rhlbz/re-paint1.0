/////////////////////////////////////   CERCHIO     /////////////////////////////////////////////////////////////////////
var selection = undefined;

function onCircleDown(){
    NotContainsCircle = true;
    if ( GraphicElements.length > 0 ){
        dragCircle = false;
        var i = 0; 
        while ( NotContainsCircle && i < GraphicElements.length ){
            if ( GraphicElements[i].type == 'circle' ){
                var r = new Rect();
                r.setX( GraphicElements[i].element.getX() - GraphicElements[i].element.getRadius());
                r.setY( GraphicElements[i].element.getY() - GraphicElements[i].element.getRadius() );
                r.setWidth( GraphicElements[i].element.getRadius() * 2);
                r.setHeight(GraphicElements[i].element.getRadius() * 2);
                if ( r.Contains(event.offsetX, event.offsetY) || r.Contains(event.offsetX - 5, event.offsetY - 5) || r.Contains(event.offsetX+5, event.offsetY+5) ){ 
                    selectedItem = i; 
                    isinCircle = true;
                    
                    selection = copyObjElement(GraphicElements[i]);
                    RepaintHistory[RepaintHistory.length-1].splice(selectedItem, 1, {'type': 'circle', 'element': selection});

                    offsetX = event.offsetX - selection.getX();
                    offsetY = event.offsetY - selection.getY();
                    selection.setBorder(true);

                    circle = new Circle();
                    
                    if ( setFill ){
                        selection.setFill(setFill);
                        if ( selectedIndexColor != undefined ){
                            selection.setColor(colors[selectedIndexColor].getColor());
                            selection.setBorderColor(colors[selectedIndexColor].getColor());
                        } else {
                            selection.setColor('black');
                            selection.setBorderColor('black');
                        }                            
                    }
                    drawSelectionBoxCircle(ctx, selection);

                    if ( circleHookTop.Contains(event.offsetX, event.offsetY) ){
                        transformEllipseTop = true;
                    }
                    
                    if (circleHookBottom.Contains(event.offsetX, event.offsetY)) {
                        transformEllipseBottom = true;
                    }

                    NotContainsCircle = false;
                } else {
                    i++;
                }
            } else {
                i++;
            }
        }
    }
    if ( NotContainsCircle ){
        selectedItem = undefined;
    }

    if ( selectedItem != undefined ){
        circleHookTop.setRadius(5);
        circleHookTop.setFill(true);
        circleHookTop.setX(selection.getX() - selection.getRadius());
        circleHookTop.setY(selection.getY() - selection.getRadius());
        circleHookTop.setColor('red');

                    
        circleHookBottom.setRadius(5);
        circleHookBottom.setFill(true);
        circleHookBottom.setX(selection.getX() + selection.getRadius());
        circleHookBottom.setY(selection.getY() + selection.getRadius());
        circleHookBottom.setColor('red');

        if ( setFill ){
            selection.setFill(true);
            selection.setColor(colors[selectedIndexColor].getColor());
            invalidate();
            drawSelectionBoxCircle(ctx, selection);
        }

        if ( selectedIndexColor != undefined ){
            selection.setBorderColor(colors[selectedIndexColor].getColor());
        } 

        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);

        var r = new Rect();
        r.setX( selection.getX() - selection.getRadius() );
        r.setY( selection.getY() - selection.getRadius()  );
        r.setWidth( selection.getRadius() );
        r.setHeight( selection.getRadius() );

        if ( circleHookTop.Contains(event.offsetX, event.offsetY) ){
            transformCircleTop = true;    
        } else {
            transformCircleTop = false;
        }
        if ( circleHookBottom.Contains(event.offsetX, event.offsetY) ) {
            transformCircleBottom = true;
        } else {
            transformCircleBottom = false;
        }
    } else {
        dragCircle = true;
        circle = new Circle();
        circle.setFirstX(event.offsetX);
        circle.setFirstY(event.offsetY);
        if ( setFill ) {
            circle.setFill(setFill);
            if ( selectedIndexColor != undefined ){
                circle.setColor(colors[selectedIndexColor].getColor());
            } else {
                circle.setColor('black');
            }
        }
    
        if ( selectedIndexColor != undefined ){
            circle.setBorderColor(colors[selectedIndexColor].getColor());
        } else {
            circle.setBorderColor('black');
        }
        
        circle.setSecondX(event.offsetX);
        circle.setSecondY(event.offsetY);
    }
}


function onCircleMove(){
    if (dragCircle && !isinCircle && !transformCircleTop && !transformCircleBottom){

        invalidate();
        circle.setSecondX(event.offsetX);
        circle.setSecondY(event.offsetY);

        var cx = (circle.getFirstX() + event.offsetX)/2;
        var cy = (circle.getFirstY() + event.offsetY)/2;

        var radius = Math.abs(event.offsetX - circle.getFirstX()) / 2;


        circle.setX(cx);
        circle.setY(cy);
        circle.setRadius(radius);

        drawSelectionBoxCircle(ctx, circle);
        drawCircle(ctx, circle);


    } 
    
    if (isinCircle && !transformCircleTop && !transformCircleBottom) {
        
        selection.setX(event.offsetX - offsetX);
        selection.setY(event.offsetY - offsetY);
        
        circleHookTop.setX( selection.getX() - selection.getRadius() );
        circleHookTop.setY(selection.getY() -  selection.getRadius());

        circleHookBottom.setX( selection.getRadius() + selection.getX() );
        circleHookBottom.setY( selection.getY() + selection.getRadius() );
        
        invalidate();
        drawSelectionBoxCircle(ctx,  selection);
        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);

    }

    if ( isinCircle && transformCircleTop ){
        
        var r = selection.getRadius();
        
        var cx = selection.getX();
        var cy = selection.getY();
        
        var differanceX = cx - r - event.offsetX;
        var differanceY = cy - r - event.offsetY;     

        if ( differanceX + r > circleHookBottom.getRadius() * 2 && differanceY + r > circleHookBottom.getRadius() * 2 ){
            selection.setX( cx - differanceX );
            selection.setY( cy - differanceY );
            invalidate();
            selection.setRadius(r + differanceX);

            circleHookTop.setX(selection.getX() - selection.getRadius() );
            circleHookTop.setY(selection.getY() - selection.getRadius() );

            circleHookBottom.setX(selection.getX() + selection.getRadius());
            circleHookBottom.setY(selection.getY() + selection.getRadius());
            invalidate();
            drawCircle(ctx, selection);
            drawSelectionBoxCircle(ctx, selection);
            drawCircle(ctx, circleHookTop);
            drawCircle(ctx, circleHookBottom);
        }

    }

    if ( isinCircle && transformCircleBottom ){
        invalidate();
        var r = selection.getRadius();
        
        var cx = selection.getX();
        var cy = selection.getY();
        
        var differanceX = event.offsetX - (cx + r);
        var differanceY = event.offsetY - (cy + r);     

        if ( differanceX + r > circleHookBottom.getRadius() * 2 && differanceY + r > circleHookBottom.getRadius() * 2 ){
            invalidate();
            selection.setX( cx + differanceX );
            selection.setY( cy + differanceY );

            selection.setRadius(r + differanceX);


            circleHookTop.setX(selection.getX() - selection.getRadius() );
            circleHookTop.setY(selection.getY() - selection.getRadius() );

            circleHookBottom.setX(selection.getX() + selection.getRadius());
            circleHookBottom.setY(selection.getY() + selection.getRadius());
            invalidate();
            drawCircle(ctx, selection);
            drawSelectionBoxCircle(ctx, selection);
            drawCircle(ctx, circleHookTop);
            drawCircle(ctx, circleHookBottom);

        }

    }

}

function onCircleUp(){
    
    dragCircle = false;
    isinCircle = false;
    transformCircleTop = false;
    transformCircleBottom = false;

    if ( !isinCircle && (circle.getRadius() > 0) ){
        circles.push(circle);
    } 

    if (selectedItem != undefined ){
        var tmp = copyGraphicElements(GraphicElements);
        tmp.splice(selectedItem, 1, {'type': 'circle', 'element': selection});
        RepaintHistory.push(tmp);
        GraphicElements.splice(selectedItem,1, {'type': 'circle', 'element': selection});
        tmp = undefined;
        selection = undefined;
    
    } else {
        GraphicElements.push({'type' : 'circle', 'element': circle});
        //RepaintHistory.push(GraphicElements);
        var tmp = copyGraphicElements(GraphicElements);
        //tmp.push({'type' : 'rect', 'element': rect});
        RepaintHistory.push(tmp);
        tmp = undefined;
        circle = undefined;
        
    }
    

}

function drawCircle(ctx, circle){
    if ( circle.getFill() ){
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.setLineDash([]);
        ctx.strokeStyle = circle.getBorderColor();
        ctx.fillStyle = circle.getColor();
        ctx.arc(circle.getX(),circle.getY(),circle.getRadius(),circle.getStartAngle(),circle.getEndAngle());
        ctx.stroke();
        ctx.fill();
        ctx.closePath();
    } else {
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.setLineDash([]);
        ctx.strokeStyle = circle.getBorderColor();
        ctx.arc(circle.getX(),circle.getY(),circle.getRadius(),circle.getStartAngle(),circle.getEndAngle());
        ctx.stroke();
        ctx.closePath();
    }
    
}

function drawSelectionBoxCircle(ctx, circle){
    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.setLineDash([5, 15]);
    ctx.strokeStyle = 'gray';
    ctx.rect(circle.getX() - circle.getRadius(), circle.getY() - circle.getRadius(), circle.getRadius() * 2, circle.getRadius() *2 );
    ctx.stroke();
    ctx.closePath();
}

