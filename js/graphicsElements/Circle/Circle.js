class Circle {

    constructor() {
        this.firstX;
        this.firstY;
        this.secondX;
        this.secondY;
        this.x; //coord x center
        this.y; //coord y center
        this.radius; //circle's radius
        this.startAngle = 0;
        this.endAngle = 2 * Math.PI;
        this.color = 'black';
        this.borderColor = 'black';
        this.border = true;
        this.fill = false;
    }

    //METHODS
    Contains(mouseX, mouseY){
        
        var distance = Math.sqrt(Math.pow((this.x-mouseX),2)+Math.pow((this.y-mouseY),2));
       
        if (distance<this.radius){
            return true;
        } else {
            return false;
        }
    }

    //GET & SET
    getX(){
        return this.x;
    }
    setX(x){
        this.x = x;
    }

    getY(){
        return this.y;
    }
    setY(y){
        this.y = y;
    }

    getFirstX(){
        return this.firstX;
    }
    setFirstX(x){
        this.firstX = x;
    }

    getFirstY(){
        return this.firstY;
    }
    setFirstY(y){
        this.firstY = y;
    }

    getSecondX(){
        return this.secondX;
    }
    setSecondX(secondX){
        this.secondX = secondX;
    }

    getSecondY(){
        return this.secondY;
    }
    setSecondY(secondY){
        this.secondY = secondY;
    }

    getRadius(){
        return this.radius;
    }
    setRadius(radius){
        this.radius = radius;
    }

    getStartAngle(){
        return this.startAngle;
    }
    setStartAngle(startAngle){
        this.startAngle = startAngle;
    }

    getEndAngle(){
        return this.endAngle;
    }
    setEndAngle(endAngle){
        this.endAngle = endAngle;
    }

    getColor(){
        return this.color;
    }
    setColor(color){
        this.color = color;
    }

    getBorder(){
        return this.border;
    }
    
    setBorder(border){
        this.border=border;
    }
    
    getBorderColor(){
        return this.borderColor;
    }
    setBorderColor(borderColor){
        this.borderColor = borderColor;
    }
    
    getFill(){
        return this.fill;
    }
    setFill(fill){
        this.fill = fill;
    }
}