class Ellipse{

    constructor(){
        this.firstX;
        this.firstY;
        this.secondX;
        this.secondY;
        this.x = 0;
        this.y = 0;
        this.radiusX = 0;
        this.radiusY = 0;
        this.rotation = 0;
        this.startAngle = 0;
        this.endAngle = 2 * Math.PI;        
        this.color = 'black';
        this.borderColor = 'black';
        this.border = true;
        this.fill = false;
        this.rotate = 0;
    }

    //METHODS
    Contains(mouseX, mouseY){
        var distX=(Math.pow((mouseX-this.x),2) ) ;
        var majorAxisQ = Math.pow(this.radiusX,2);
        var distY=(Math.pow((mouseY-this.y),2) );
        var minorAsisQ = Math.pow(this.radiusY,2);
        
        if (((distX/majorAxisQ)+(distY/minorAsisQ))<=1){
            return true;
        } else {
            return false;
        }
    }

    getBorder(){
        return this.border;
    }
    
    setBorder(border){
        this.border=border;
    }

    getFirstX(){
        return this.firstX;
    }
    setFirstX(x){
        this.firstX = x;
    }

    getFirstY(){
        return this.firstY;
    }
    setFirstY(y){
        this.firstY = y;
    }

    getSecondX(){
        return this.secondX;
    }
    setSecondX(secondX){
        this.secondX = secondX;
    }

    getSecondY(){
        return this.secondY;
    }
    setSecondY(secondY){
        this.secondY = secondY;
    }

    getX(){
        return this.x
    }
    setX(x){
        this.x = x;
    }

    getY(){
        return this.y;
    }
    setY(y){
        this.y = y;
    }

    getRadiusX() {
        return this.radiusX;
    }
    setRadiusX(radiusX){
        this.radiusX = radiusX;
    }

    getRadiusY(){
        return this.radiusY;
    }
    setRadiusY(radiusY){
        this.radiusY = radiusY;
    }

    getRotation(){
        return this.rotation;
    }
    setRotation(rotation){
        this.rotation = rotation;
    }

    getStartAngle(){
        return this.startAngle;
    }
    setStartAngle(startAngle){
        this.startAngle = startAngle;
    }

    getEndAngle(){
        return this.endAngle;
    }
    setEndAngle(endAngle){
        this.endAngle = endAngle;
    }

    getColor(){
        return this.color;
    }
    setColor(color){
        this.color = color;
    }

    getBorderColor(){
        return this.borderColor;
    }
    setBorderColor(borderColor){
        this.borderColor = borderColor;
    }

    getFill(){
        return this.fill;
    }
    setFill(fill){
        this.fill = fill;
    }

    getRotate(){
        return this.rotate;
    }
    setRotate(rotate){
        this.rotate = rotate;
    }

}