/////////////////////////////////////   ELLISSE     /////////////////////////////////////////////////////////////////////
var selection = undefined;

function onEllipseDown(){
    NotContainsEllipse=true;
    if ( GraphicElements.length > 0 ){
        dragEllipse = false;
        var i = 0; 
        while(NotContainsEllipse && i < GraphicElements.length){
            if (GraphicElements[i].type == 'ellipse' ){
                var r = new Rect();
                r.setX( GraphicElements[i].element.getX() - GraphicElements[i].element.getRadiusX());
                r.setY( GraphicElements[i].element.getY() - GraphicElements[i].element.getRadiusY());
                r.setWidth( GraphicElements[i].element.getRadiusX()*2);
                r.setHeight( GraphicElements[i].element.getRadiusY()*2);

                if ( r.Contains(event.offsetX, event.offsetY) || r.Contains(event.offsetX - 5, event.offsetY - 5) || r.Contains(event.offsetX + 5, event.offsetY + 5 ) ){
                    selectedItem = i;
                    isinEllipse = true;
                    selection = copyObjElement(GraphicElements[i]);
                    RepaintHistory[RepaintHistory.length-1].splice(selectedItem, 1, {'type': 'ellipse', 'element': selection});
                    offsetX = event.offsetX - selection.getX();
                    offsetY = event.offsetY - selection.getY();
                    ellipse = new Ellipse();
                    if ( setFill ){
                        selection.setFill(setFill);
                        if ( selectedIndexColor != undefined ){
                            selection.setColor(colors[selectedIndexColor].getColor());
                            selection.setBorderColor(colors[selectedIndexColor].getColor());
                        } else {
                            selection.setColor('black');
                            selection.setBorderColor(selection.getColor());
                        }
                    }
                    drawSelectionBoxEllipse(ctx, selection);
                    
                    if ( circleHookTop.Contains(event.offsetX, event.offsetY) ){
                        transformEllipseTop = true;
                    }
                    
                    if (circleHookBottom.Contains(event.offsetX, event.offsetY)) {
                        transformEllipseBottom = true;
                    }

                    NotContainsEllipse=false;      

                } else {
                    i++;
                }
            } else {
                i++;
            }
        }
        if(NotContainsEllipse){
            selectedItem = undefined;
        }  
    }
    
    if ( selectedItem != undefined ){
        circleHookTop.setRadius(5);
        circleHookTop.setFill(true);
        circleHookTop.setX(selection.getX() - selection.getRadiusX());
        circleHookTop.setY(selection.getY() - selection.getRadiusY());
        circleHookTop.setColor('red');

                    
        circleHookBottom.setRadius(5);
        circleHookBottom.setFill(true);
        circleHookBottom.setX(selection.getX() + selection.getRadiusX());
        circleHookBottom.setY(selection.getY() + selection.getRadiusY());
        circleHookBottom.setColor('red');

        if ( setFill ){
            selection.setFill(true);
            selection.setColor(colors[selectedIndexColor].getColor());
            invalidate();
            drawSelectionBoxEllipse(ctx, selection);
        }

        if ( selectedIndexColor != undefined ){
            selection.setBorderColor(colors[selectedIndexColor].getColor());
        } 

        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);

        var r = new Rect();
        r.setX( GraphicElements[i].element.getX() - GraphicElements[i].element.getRadiusX());
        r.setY( GraphicElements[i].element.getY() - GraphicElements[i].element.getRadiusY());
        r.setWidth( GraphicElements[i].element.getRadiusX()*2);
        r.setHeight( GraphicElements[i].element.getRadiusY()*2);

        if ( circleHookTop.Contains(event.offsetX, event.offsetY) ){
            transformEllipseTop = true;    
        } else {
            transformEllipseTop = false;
        }
        if ( circleHookBottom.Contains(event.offsetX, event.offsetY) ) {
            transformEllipseBottom = true;
        } else {
            transformEllipseBottom = false;
        }
    } else {
        dragEllipse = true;
        ellipse = new Ellipse();
        ellipse.setFirstX(event.offsetX);
        ellipse.setFirstY(event.offsetY); 
        if ( setFill ){
            ellipse.setFill(setFill)
            if ( selectedIndexColor != undefined ){
                ellipse.setColor(colors[selectedIndexColor].getColor());
            } else {
                ellipse.setColor('black');
            }
        }
        if ( selectedIndexColor != undefined ){
            ellipse.setBorderColor(colors[selectedIndexColor].getColor());
        }
        ellipse.setSecondX(event.offsetX);
        ellipse.setSecondY(event.offseY);

    }
}

function onEllipseMove() {
    if (dragEllipse && !isinEllipse && !transformEllipseBottom && !transformEllipseTop){
        invalidate();
        ellipse.setSecondX(event.offsetX);
        ellipse.setSecondY(event.offsetY);

        var cx = (ellipse.getFirstX() + event.offsetX) / 2;
        var cy = (ellipse.getFirstY() + event.offsetY) / 2;

        var axis1 = Math.abs(event.offsetX - ellipse.getFirstX()) / 2;
        var axis2 = Math.abs(event.offsetY - ellipse.getFirstY()) / 2;

        var radiusX;
        var radiusY;

        if (axis1 > axis2) {
            radiusX = axis1;
            radiusY = axis2;
        } else {
            radiusX = axis2;
            radiusY = axis1;
        }

        if((Math.abs(ellipse.firstX-ellipse.secondX))>(Math.abs(ellipse.firstY-ellipse.secondY))){
            ellipse.setRotation(0);
        } else{
            ellipse.setRotation(Math.PI/2);
        } ////// FUNZIONE NELLA ONELIPSEMOVE CHE SETTA LA ROTAZIONE DELL'ELLISSE IN BASE AL MOVIMENTO DEL MOUSE

        ellipse.setX(cx);
        ellipse.setY(cy);
        ellipse.setRadiusX(radiusX);
        ellipse.setRadiusY(radiusY);


        drawSelectionBoxEllipse(ctx, ellipse);
        drawEllipse(ctx, ellipse);

    } 
    
    if ( isinEllipse && !transformEllipseTop && !transformEllipseBottom) {
        selection.setX(event.offsetX - offsetX);
        selection.setY(event.offsetY - offsetY);
        
        circleHookTop.setX(selection.getX() - selection.getRadiusX());
        circleHookTop.setY(selection.getY() - selection.getRadiusY());

        circleHookBottom.setX(circleHookTop.getX() + (selection.getRadiusX() * 2));
        circleHookBottom.setY(circleHookTop.getY() + (selection.getRadiusY() * 2));

        invalidate();
        drawSelectionBoxEllipse(ctx, selection);
        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);
    }

    if ( isinEllipse && transformEllipseTop ){

        invalidate();

        var rx = selection.getRadiusX();
        var ry = selection.getRadiusY();

        var cx = selection.getX();
        var cy = selection.getY();

        var differanceX = cx - rx - event.offsetX;
        var differanceY = cy - ry - event.offsetY;


        if ( differanceX + rx > circleHookBottom.getRadius() * 2 && differanceY + ry > circleHookBottom.getRadius() * 2 ) {

            selection.setX( cx - differanceX );
            selection.setY( cy - differanceY );
            
            selection.setRadiusX(rx + differanceX);
            selection.setRadiusY(ry + differanceY);

            if((Math.abs(selection.getFirstX() - selection.getSecondX()))>(Math.abs( selection.getFirstY() - selection.getSecondY()))){
                selection.setRotation(0);
            } else{
                selection.setRotation(Math.PI/2);
            }

            circleHookTop.setX(selection.getX() - selection.getRadiusX());
            circleHookTop.setY(selection.getY() - selection.getRadiusY());
            
            selection.setFirstX(event.offsetX);
            selection.setFirstY(event.offsetY);

            invalidate();
            drawEllipse(ctx, selection);
            drawSelectionBoxEllipse(ctx, selection);
            drawCircle(ctx, circleHookTop);
            drawCircle(ctx, circleHookBottom);
        }

    }

    if ( isinEllipse && transformEllipseBottom ){
        
        invalidate();
        
        var rx = selection.getRadiusX();
        var ry = selection.getRadiusY();

        var cx = selection.getX();
        var cy = selection.getY();
        
        var differanceX = event.offsetX - (cx + rx);
        var differanceY = event.offsetY - (cy + ry);     

        if ( differanceX + rx > circleHookBottom.getRadius() * 2 && differanceY + ry > circleHookBottom.getRadius() * 2 ) {


            selection.setX( cx + differanceX );
            selection.setY( cy + differanceY );
            
            selection.setRadiusX(rx + differanceX);
            selection.setRadiusY(ry + differanceY);
            
        if((Math.abs(selection.getFirstX() - selection.getSecondX()))>(Math.abs( selection.getFirstY() - selection.getSecondY()))){
                selection.setRotation(0);
            } else{
                selection.setRotation(Math.PI/2);
            }

            circleHookBottom.setX( selection.getX() + selection.getRadiusX());
            circleHookBottom.setY(selection.getY() + selection.getRadiusY());

            selection.setSecondX(event.offsetX);
            selection.setSecondY(event.offsetY);

            drawEllipse(ctx, selection);
            drawSelectionBoxEllipse(ctx, selection);
            drawCircle(ctx, circleHookTop);
            drawCircle(ctx, circleHookBottom);
        }

    } 
}

function onEllipseUp(){  
   
    dragEllipse = false;
    isinEllipse = false;
    transformEllipseTop = false;
    transformEllipseBottom = false;

    if (!isinEllipse && (ellipse.getRadiusX() != 0) && (ellipse.getRadiusY() != 0)){
        
        ellipses.push(ellipse);   
        
    }

    if (selectedItem != undefined ){
        var tmp = copyGraphicElements(GraphicElements);
        tmp.splice(selectedItem, 1, {'type': 'ellipse', 'element': selection});
        RepaintHistory.push(tmp);
        GraphicElements.splice(selectedItem,1, {'type': 'ellipse', 'element': selection});
        tmp = undefined;
        selection = undefined;
    
    } else {
        GraphicElements.push({'type' : 'ellipse', 'element': ellipse});
        //RepaintHistory.push(GraphicElements);
        var tmp = copyGraphicElements(GraphicElements);
        //tmp.push({'type' : 'rect', 'element': rect});
        RepaintHistory.push(tmp);
        tmp = undefined;
        ellipse = undefined;
        
    }
    
}

function drawEllipse(ctx, ellipse){
    ctx.beginPath();
    ctx.save();
    if(ellipse.getFill() ){
        if ( ellipse.getRotate() != 0 ){
            ctx.translate(ellipse.getX(), ellipse.getY());
            ctx.rotate(ellipse.getRotate())
            ctx.lineWidth = 1;
            ctx.setLineDash([]);
            ctx.strokeStyle = ellipse.getBorderColor();
            ctx.fillStyle = ellipse.getColor();
            ctx.ellipse( 0,0, ellipse.getRadiusX(), ellipse.getRadiusY(), ellipse.getRotation(), ellipse.getStartAngle(), ellipse.getEndAngle());
            ctx.stroke();
            ctx.fill();
            ctx.restore();
            ctx.closePath();
        } else {
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.setLineDash([]);
            ctx.strokeStyle = ellipse.getBorderColor();
            ctx.fillStyle = ellipse.getColor();
            ctx.ellipse(ellipse.getX(), ellipse.getY(), ellipse.getRadiusX(), ellipse.getRadiusY(), ellipse.getRotation(), ellipse.getStartAngle(), ellipse.getEndAngle());
            ctx.stroke();
            ctx.fill();
            ctx.restore();
            ctx.closePath();
        }
    } else {
        if ( ellipse.getRotate() != 0 ){
            ctx.beginPath();
            ctx.translate(ellipse.getX(), ellipse.getY());
            ctx.rotate(ellipse.getRotate());
            ctx.lineWidth = 1;
            ctx.setLineDash([]);
            ctx.strokeStyle = ellipse.getBorderColor();
            ctx.ellipse( 0,0, ellipse.getRadiusX(), ellipse.getRadiusY(), ellipse.getRotation(), ellipse.getStartAngle(), ellipse.getEndAngle());
            ctx.stroke();
            ctx.restore();  
            ctx.closePath();
        } else {
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.setLineDash([]);
            ctx.strokeStyle = ellipse.getBorderColor();
            ctx.ellipse(ellipse.getX(), ellipse.getY(), ellipse.getRadiusX(), ellipse.getRadiusY(), ellipse.getRotation(), ellipse.getStartAngle(), ellipse.getEndAngle());
            ctx.stroke();
            ctx.restore();
            ctx.closePath();
        }
    }
    ctx.closePath();
    ctx.restore();
}

function drawSelectionBoxEllipse(ctx, ellipse) { 
    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.setLineDash([5, 15]);
    ctx.strokeStyle = 'gray';
    if (ellipse.getRotation()==0){
        ctx.rect((ellipse.getX()-ellipse.getRadiusX()),(ellipse.getY()-ellipse.getRadiusY()),(ellipse.getRadiusX()*2),(ellipse.getRadiusY()*2));
    }else{
        ctx.rect((ellipse.getX()-ellipse.getRadiusX()),(ellipse.getY()-ellipse.getRadiusY()),(ellipse.getRadiusX()*2),(ellipse.getRadiusY()*2));
    }
    ctx.stroke();   
    ctx.closePath();
}