///////////////////////////////////////////   RETTANGOLO   /////////////////////////////////////////////////////////////////////
var selection = undefined;
var click;

function onTextDown(){ 
    //invalidate();
    NotContainsText=true;
    if ( GraphicElements.length > 0 ){
        dragText = false;
        var i = 0; 
        while(NotContainsText && i < GraphicElements.length){
            if (GraphicElements[i].type == 'text'){
                if ( GraphicElements[i].element.Contains(event.offsetX, event.offsetY)){
                    selectedItem = i;
                    isinText = true;
                    
                    selection = copyObjElement(GraphicElements[i]);
                    RepaintHistory[RepaintHistory.length-1].splice(selectedItem, 1, {'type': 'text', 'element': selection});
                    if ( selectedIndexColor != undefined ) {
                        selection.setColor(colors[selectedIndexColor].getColor());
                        invalidate();
                    }
                    offsetX = event.offsetX - selection.getX();
                    offsetY = event.offsetY - selection.getY();
                    selection.setBorder(true);
                    drawSelectionBoxText(ctx, selection);
                    text = new Text();
                    NotContainsText=false;
                } else {
                    i++;
                } 
            } else {
                i++;
            }
        }
        if(NotContainsText) {
            selectedItem = undefined; 
        }    
    }    
    
    
    if ( selectedItem != undefined ){
        $("#editText").css("display", "inline-block");
        if ( selectedIndexColor != undefined ){
            selection.setColor(colors[selectedIndexColor].getColor());
            invalidate();
        } 
    } else {
        dragText = true;
        text = new Text();
        $("#editText").css("display", "none");
        onClickText();
        if ( selectedIndexColor != undefined ){
            text.setColor(colors[selectedIndexColor].getColor());
        }
        
        $("#inputText").val('');
        $("#inputTextSize").val(20);
        drawSelectionBoxText(ctx, text);
        offsetX = undefined;
        offsetY = undefined;
    }
   
}

function onTextMove(){
    if ( isinText && selectedItem != undefined) {
        //invalidate();
        selection.setX(event.offsetX - offsetX);
        selection.setY(event.offsetY - offsetY);
        selection.setWidth(event.offsetX - selection.getX() - offsetX);
        selection.setHeight(event.offsetY - selection.getY() - offsetY);
        invalidate();
        drawText(ctx, selection);   
        drawSelectionBoxText(ctx, selection);
    }
}

function onTextUp(){

    dragText = false;
    isinText = false;
    click = false;
    if (  selectedItem == undefined ) {
        if ( (!isinText) ){
            text = new Text();
            text.setX(event.offsetX);
            text.setY(event.offsetY);
            text.setmaxWidth(250);
            text.setFontSize(20);
            
            text.setHeight( event.offsetY - text.getY() );
            text.setWidth(event.offsetX - text.getX());
            drawSelectionBoxText(ctx,text);
            texts.push(text);
            GraphicElements.push({'type' : 'text', 'element': text});
            var tmp = copyGraphicElements(GraphicElements);
            RepaintHistory.push(tmp);
            tmp = undefined;
            text = undefined;
        }
    } else {
        drawSelectionBoxText(ctx,selection);
        var tmp = copyGraphicElements(GraphicElements);
        tmp.splice(selectedItem, 1, {'type': 'text', 'element': selection});
        RepaintHistory.push(tmp);
        GraphicElements.splice(selectedItem,1, {'type': 'text', 'element': selection});
        tmp = undefined;
        selection = undefined;
    }
}

function onSetText(){  

    var inputText = document.getElementById("inputText").value;
    var inputTextSize = document.getElementById("inputTextSize").value;
    var inputTextFamily = document.getElementById("inputTextFamily").value;

    if ( selectedIndexColor != undefined ){
        GraphicElements[GraphicElements.length-1].element.setColor(colors[selectedIndexColor].getColor());    
    }
    
    GraphicElements[GraphicElements.length-1].element.setBold(bold);
    GraphicElements[GraphicElements.length-1].element.setItalic(italic);
    GraphicElements[GraphicElements.length-1].element.setUnderline(underline);
    
    GraphicElements[GraphicElements.length-1].element.setText(inputText);
    GraphicElements[GraphicElements.length-1].element.setFontSize(Number(inputTextSize));
    GraphicElements[GraphicElements.length-1].element.setFontFamily(inputTextFamily);
    GraphicElements[GraphicElements.length-1].element.setFontStyle(inputTextSize, inputTextFamily);
    var maxWidth = ctx.measureText(GraphicElements[GraphicElements.length-1].element.getText() * Number(inputTextSize) );
    GraphicElements[GraphicElements.length-1].element.setmaxWidth(maxWidth.width * inputText.length);
    if ( selectedIndexColor == undefined ){
        GraphicElements[GraphicElements.length-1].element.setColor("black");
    }
    var tmp = copyGraphicElements(GraphicElements);
    RepaintHistory[RepaintHistory.length-1] = tmp;
    invalidate();
    tmp = undefined;
    onCloseSetupText();

}

function onChangeText(){
    var selectedElement = copyObjElement(GraphicElements[selectedItem]);

    var inputText = document.getElementById("inputText").value;
    var inputTextSize = document.getElementById("inputTextSize").value;
    var inputTextFamily = document.getElementById("inputTextFamily").value;

    selectedElement.setBold(bold);
    selectedElement.setItalic(italic);
    selectedElement.setUnderline(underline);

    selectedElement.setText(inputText);
    selectedElement.setFontSize(Number(inputTextSize));
    selectedElement.setFontFamily(inputTextFamily);
    selectedElement.setFontStyle(inputTextSize, inputTextFamily);
    selectedElement.setmaxWidth(inputText.length * (inputTextSize / 2));
    if ( selectedIndexColor != undefined ){
        selectedElement.setColor(colors[selectedIndexColor].getColor());
    }
    var tmp = copyGraphicElements(GraphicElements);
    tmp.splice(selectedItem, 1, {'type': 'text', 'element': selectedElement});
    RepaintHistory.push(tmp);
    GraphicElements.splice(selectedItem, 1, {'type':'text', 'element': selectedElement});
    invalidate();
    $("#editText").css("display", "none");
    selectedElement = undefined;
    tmp = undefined;
    onCloseSetupText();
}

function onSetBold(){
    if ( !bold ){
        bold = true;
        $("#boldButton").addClass("active");
    } else {
        bold = false;
        $("#boldButton").removeClass("active");
    }
}

function onSetItalic(){
    if ( !italic ){
        italic = true;
        $("#italicButton").addClass("active");
    } else {
        italic = false;
        $("#italicButton").removeClass("active");
    }
}

function onSetUnderline(){
    if ( !underline ){
        underline = true;
        $("#underlineButton").addClass("active");
    } else {
        underline = false;
        $("#underlineButton").removeClass("active");
    }
}

function drawText(ctx , text){   
    ctx.beginPath();
    ctx.save();
    ctx.font=text.getFontStyle();
    ctx.fillStyle = text.getColor();
    text.setmaxWidth(ctx.measureText(text.getText()).width );
    ctx.fillText(text.getText(), text.getX(), text.getY(), text.getmaxWidth());
    if ( text.getUnderline()){
        ctx.strokeStyle = text.getColor();
        ctx.moveTo(text.getX(), text.getY());
        ctx.lineTo(text.getX() + text.getmaxWidth(), text.getY());
        ctx.lineWidth = 1;
        ctx.setLineDash([]);
        ctx.stokeStyle = text.getColor();
        ctx.stroke();
    } 
    ctx.closePath();
    ctx.restore();
    
}

function drawSelectionBoxText(ctx, text) { 
    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.setLineDash([5, 15]);
    ctx.strokeStyle = 'gray';
    ctx.rect(text.getX(),text.getY(),text.getmaxWidth(),-text.getfontSize());
    ctx.stroke();   
}
