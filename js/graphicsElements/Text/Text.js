class Text {

    constructor() {
        this.text = "";
        this.x;
        this.y;
        this.maxWidth;
        this.fontSize = 20;
        this.fontFamily = 'Arial';
        this.fontStyle = "20px Arial";
        //this.font=this.fontSize+'px arial';
        this.border = false;
        this.color = "rgba(197,197,197,0.7)";
        this.height;
        this.maxWidth;
        this.rotate = 0;
        this.bold;
        this.italic;
        this.underline;
    }

    //METHODS
    Contains(mouseX, mouseY){

        this.height=-this.fontSize;
        
        if((this.maxWidth>0)&&(this.height>0)){ //verso destra e basso
            if ( (mouseX > this.x) && (mouseX < (this.maxWidth + this.x )) && (mouseY > this.y) && (mouseY < (this.height + this.y)) ) {
                return true;
            } else {
                return false;
            }
        } else if ((this.maxWidth>0)&&(this.height<0)){ //verso destra e alto
            if ( (mouseX > this.x) && (mouseX < (this.maxWidth + this.x )) && (mouseY < this.y) && (mouseY > (this.height + this.y)) ) {
                return true;
            } else {
                return false;
            }
        }else if ((this.maxWidth<0)&&(this.height>0)){ //verso sinistra e basso
            if ( (mouseX < this.x) && (mouseX > (this.maxWidth + this.x )) && (mouseY > this.y) && (mouseY < (this.height + this.y)) ) {
                return true;
            } else {
                return false;
            }
        }else if ((this.maxWidth<0)&&(this.height<0)){ //verso sinistra e alto
            if ( (mouseX < this.x) && (mouseX > (this.maxWidth + this.x )) && (mouseY < this.y) && (mouseY > (this.height + this.y)) ) {
                return true;
            } else {
                return false;
            }
        }    
    }


    getText(){
        return this.text;
    }

    setText(text){
        this.text=text;
    }

    // GET & SET
    getX (){
        return this.x;
    }
    setX (x) {
        this.x = x;
    }
    
    getY(){
        return this.y;
    }
    setY (y) {
        this.y = y;
    }

    getWidth(){
        return this.width;
    }
    setWidth(width){
        this.width = width;
    }

    getHeight(){
        return this.height;
    }
    setHeight(height){
        this.height = height;
    }
    
    getmaxWidth(){
        return this.maxWidth;
    }
    setmaxWidth (maxWidth){
        this.maxWidth = maxWidth;
    }
    
    getfontSize(){
        return this.fontSize;
    }
    
    setfontSize (fontSize) {
        this.fontSize = fontSize;
    }

    getFontFamily(){
        return this.fontFamily;
    }
    setFontFamily(fontFamily){
        this.fontFamily = fontFamily;
    }

    getFontSize(){
        return this.fontSize;
    }
    setFontSize(fontSize){
        this.fontSize = fontSize;
    }

    getFontStyle(){
        return this.fontStyle;
    }

    setFontStyle(fontSize, fontFamily){
        var styling = "";
        if ( this.italic ){
            styling += "italic ";
        } 
        if ( this.bold ){
            styling += "bold ";
        }
        styling += fontSize + 'px ' + fontFamily ;

        this.fontStyle = styling;
    }


    getColor(){
        return this.color;
    }
    setColor(color){
        this.color = color;
    }

    getText(){
        return this.text;
    }

    setText(text){
        this.text=text;
    }
    getBorder(){
        return this.border;
    }
    setBorder(border){
        this.border = border;
    }

    getRotate(){
        return this.rotate;
    }
    setRotate(rotate){
        this.rotate = rotate;
    }

    getBold(){
        return this.bold;
    }
    setBold(bold){
        this.bold = bold;
    }

    getItalic(){
        return this.italic;
    }
    setItalic(italic){
        this.italic = italic;
    }

    getUnderline(){
        return this.underline;
    }
    setUnderline(underline){
        this.underline = underline;
    }
}