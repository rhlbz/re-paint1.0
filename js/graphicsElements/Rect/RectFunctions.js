///////////////////////////////////////////   RETTANGOLO   /////////////////////////////////////////////////////////////////////
var selection = undefined; 

function onRectDown(){ 
    NotContainsRect=true;
    if ( GraphicElements.length > 0 ){
        dragRect = false;
        var i = 0; 
        while(NotContainsRect && i < GraphicElements.length){
            if (GraphicElements[i].type == 'rect'){                
                if ( (GraphicElements[i].element.Contains(event.offsetX, event.offsetY)) || (GraphicElements[i].element.Contains(event.offsetX - 5, event.offsetY-5)) || (GraphicElements[i].element.Contains(event.offsetX+5, event.offsetY+5)) ){
                    selectedItem = i;
                    isinRect = true;
                    
                    selection = copyObjElement(GraphicElements[i]);
                    RepaintHistory[RepaintHistory.length-1].splice(selectedItem, 1, {'type': 'rect', 'element': selection});
                    offsetX = event.offsetX - selection.getX();
                    offsetY = event.offsetY - selection.getY();
                    rect = new Rect();
                    if ( setFill ){
                        selection.setFill(setFill);
                        if ( selectedIndexColor != undefined ) {
                            selection.setColor(colors[selectedIndexColor].getColor());
                            selection.setBorderColor(selection.getColor());
                        } else {
                            selection.setColor('black');
                            selection.setBorderColor(selection.getColor());
                        }
                    } 

                    invalidate();
                    NotContainsRect=false;
                } else {
                    i++;
                } 
            } else {
                i++;
            }
        }
        if(NotContainsRect) {
            selectedItem = undefined;
        }    
    }

    if ( selectedItem != undefined ){        
        invalidate();
        drawSelectionBoxRect(ctx, selection);
        circleHookTop.setRadius(5);
        circleHookTop.setFill(true);
        circleHookTop.setX(selection.getX());
        circleHookTop.setY(selection.getY());
        circleHookTop.setColor('red');

                    
        circleHookBottom.setRadius(5);
        circleHookBottom.setFill(true);
        circleHookBottom.setX(selection.getX() + selection.getWidth());
        circleHookBottom.setY(selection.getY() + selection.getHeight());
        circleHookBottom.setColor('red');

        if ( setFill ){
            selection.setFill(true);
            selection.setColor(colors[selectedIndexColor].getColor());
            selection.setBorderColor(colors[selectedIndexColor].getColor());
            invalidate();
        }

        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);

        if ( circleHookTop.Contains(event.offsetX, event.offsetY) ){
            transformRectTop = true;    
            document.body.style.cursor = "se-resize";
        } else {
            transformRectTop = false;
            document.body.style.cursor = "default";
        }
        if ( circleHookBottom.Contains(event.offsetX, event.offsetY) ) {
            transformRectBottom = true;
            document.body.style.cursor = "se-resize";
        }   else {
            transformRectBottom = false;
            document.body.style.cursor = "default";
        }
    } else {
        dragRect = true;
        rect = new Rect();
        ctx.restore();
        rect.setX(event.offsetX);
        rect.setY(event.offsetY);

        if ( setFill ) {
            rect.setFill(setFill);
            if ( selectedIndexColor != undefined ){
                rect.setColor(colors[selectedIndexColor].getColor());
            } else {
                rect.setColor('black');
            }
        }
        if ( selectedIndexColor != undefined ){
            rect.setBorderColor(colors[selectedIndexColor].getColor());
        } else {
            rect.setBorderColor('black');
        }

        rect.setWidth(0);
        rect.setHeight(0);

        drawSelectionBoxRect(ctx, rect);
        
        }
   
    
}

function onRectMove(){
    if (dragRect && !isinRect && !transformRectTop && !transformRectBottom) {
        if ( regular ){
            rect.setWidth(( event.offsetX) - rect.getX());
            var w = rect.getWidth();
            rect.setHeight( w );
        } else {
            rect.setWidth((event.offsetX) - rect.getX());
            rect.setHeight((event.offsetY) - rect.getY());
        }
        invalidate();
        drawRect(ctx, rect);
        drawSelectionBoxRect(ctx, rect);  
    } 

    if ( isinRect && !transformRectTop && !transformRectBottom ) {
        

        selection.setX( event.offsetX - offsetX);
        selection.setY( event.offsetY - offsetY);

        circleHookTop.setX( selection.getX());
        circleHookTop.setY( selection.getY());

        circleHookBottom.setX(selection.getX() + selection.getWidth());
        circleHookBottom.setY(selection.getY() + selection.getHeight());
        invalidate();
        drawRect(ctx, selection);   
        drawSelectionBoxRect(ctx, selection);
        drawCircle(ctx, circleHookTop);
        drawCircle(ctx, circleHookBottom);
        
    } 

    if ( isinRect && transformRectTop ){
        
        var newWidth = selection.getWidth() + (selection.getX() - event.offsetX);
        var newHeight = selection.getHeight() +(selection.getY() - event.offsetY);
        invalidate();
        if ( newWidth > circleHookBottom.getRadius() * 2 && newHeight > circleHookBottom.getRadius() * 2 ) {
            if ( regular ){
                selection.setWidth(selection.getWidth() + (selection.getX() - event.offsetX) );
                selection.setHeight(selection.getWidth());
        
                selection.setX(event.offsetX);
                selection.setY(event.offsetY);
        
                circleHookTop.setX( selection.getX());
                circleHookTop.setY( selection.getY());
        
                circleHookBottom.setX(selection.getX() + selection.getWidth());
                circleHookBottom.setY(selection.getY() + selection.getHeight());
                invalidate();
                drawRect(ctx,selection);  
                drawSelectionBoxRect(ctx, selection);
                drawCircle(ctx, circleHookTop);
                drawCircle(ctx, circleHookBottom);

            } else {
                selection.setWidth(selection.getWidth() + (selection.getX() - event.offsetX) );
                selection.setHeight(selection.getHeight() +(selection.getY() - event.offsetY) );
        
                selection.setX(event.offsetX);
                selection.setY(event.offsetY);
        
                circleHookTop.setX( selection.getX());
                circleHookTop.setY( selection.getY());
        
                circleHookBottom.setX(selection.getX() + selection.getWidth());
                circleHookBottom.setY(selection.getY() + selection.getHeight());
                invalidate();
                drawRect(ctx,selection);  
                drawSelectionBoxRect(ctx, selection);
                drawCircle(ctx, circleHookTop);
                drawCircle(ctx, circleHookBottom);
            }
            
        }

    }

    if ( isinRect && transformRectBottom ){

       
        var newWidth = event.offsetX - selection.getX();
        var newHeight = event.offsetY - selection.getY();
        invalidate();
        if ( newWidth > circleHookBottom.getRadius() * 2 && newHeight > circleHookBottom.getRadius() * 2 ){
            if ( regular ) {
                selection.setWidth( event.offsetX - selection.getX());
                selection.setHeight(selection.getWidth());
                circleHookTop.setX( selection.getX());
                circleHookTop.setY( selection.getY());
        
                circleHookBottom.setX(selection.getX() + selection.getWidth());
                circleHookBottom.setY(selection.getY() + selection.getHeight());
                
                drawRect(ctx,selection);
                drawSelectionBoxRect(ctx, selection);

                drawCircle(ctx, circleHookTop);
                drawCircle(ctx, circleHookBottom);
            } else {
                selection.setWidth( event.offsetX - selection.getX());
                selection.setHeight( event.offsetY - selection.getY() );
                circleHookTop.setX( selection.getX());
                circleHookTop.setY( selection.getY());
        
                circleHookBottom.setX(selection.getX() + selection.getWidth());
                circleHookBottom.setY(selection.getY() + selection.getHeight());
                
                drawRect(ctx,selection);
                drawSelectionBoxRect(ctx, selection);
    
                drawCircle(ctx, circleHookTop);
                drawCircle(ctx, circleHookBottom);
            }
            
        } else {
            dragRect = false;
        }
    }
}

function onRectUp(){

    dragRect = false;
    isinRect = false;
    transformRectTop = false;
    transformRectBottom = false;
    

    //NB: FONDAMENTALE ORDINE DELLA SOTTRAZIONE
    if ( regular ){
        if ( (!isinRect) && (rect.getWidth() != 0) && (rect.getHeight() != 0) ){
            rect.setEndX(event.offsetX);
            rect.setEndY(event.offsetY);
            rect.setWidth( event.offsetX - rect.getX() );
            var w = rect.getWidth()
            rect.setHeight( w );
            rects.push(rect);
        } 
    } else {
        if ( (!isinRect) && (rect.getWidth() != 0) && (rect.getHeight() != 0) ){
            rect.setEndX(event.offsetX);
            rect.setEndY(event.offsetY);
            rect.setWidth( event.offsetX - rect.getX() );
            rect.setHeight( event.offsetY - rect.getY() );
            rects.push(rect);
        } 
    } 
    
    if (selectedItem != undefined ){
        var tmp = copyGraphicElements(GraphicElements);
        tmp.splice(selectedItem, 1, {'type': 'rect', 'element': selection});
        RepaintHistory.push(tmp);
        GraphicElements.splice(selectedItem,1, {'type': 'rect', 'element': selection});
        tmp = undefined;
        selection = undefined;
    
    } else {
        GraphicElements.push({'type' : 'rect', 'element': rect});
        //RepaintHistory.push(GraphicElements);
        var tmp = copyGraphicElements(GraphicElements);
        //tmp.push({'type' : 'rect', 'element': rect});
        RepaintHistory.push(tmp);
        tmp = undefined;
        rect = undefined;
        
    }
    regular = false;


}

function drawRect(ctx , rect){
    
    ctx.beginPath();
    ctx.save();
    if ( rect.getFill() ) {
        ctx.setLineDash([]);
        ctx.lineWidth = 1;
        ctx.strokeStyle = rect.getBorderColor();
        ctx.fillStyle = rect.getColor();
        ctx.rect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
        ctx.fillRect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
        ctx.stroke();
        ctx.restore();
        ctx.closePath();
    } else {
        if (rect.getRotate() != 0 ){
            ctx.beginPath();
            ctx.translate( ((rect.getWidth() + rect.getX())/2), ((rect.getHeight() + rect.getY())/2));
            ctx.transform( (Math.cos(rect.getRotate())), (Math.sin(rect.getRotate())), -(Math.sin(rect.getRotate())), (Math.cos(rect.getRotate())), 0,0);
            ctx.setLineDash([]);
            ctx.lineWidth = 1;
            ctx.strokeStyle = rect.getBorderColor();
            ctx.fillStyle = rect.getColor();
            ctx.rect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
            ctx.stroke();
            ctx.restore();
            ctx.closePath();
        } else {
            ctx.setLineDash([]);
            ctx.lineWidth = 1;
            ctx.strokeStyle = rect.getBorderColor();
            ctx.fillStyle = rect.getColor();
            ctx.rect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
            ctx.stroke();
            ctx.closePath();
        }
        
    }
    ctx.closePath();
    ctx.restore();
}

function drawSelectionBoxRect(ctx, rect) { 
    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.setLineDash([5, 15]);
    ctx.strokeStyle = 'gray';
    ctx.rect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
    ctx.stroke();   
    ctx.closePath();
}
