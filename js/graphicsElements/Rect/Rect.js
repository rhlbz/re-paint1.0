class Rect {

    constructor() {
        this.x = 0;
        this.y = 0;
        this.width = 50;
        this.height = 50;
        this.border = true;
        this.fill = false;
        this.color;
        this.borderColor = 'black';
        this.endX;
        this.endY;
        this.rotate = 0;
    }

    //METHODS
    Contains(mouseX, mouseY){
        if((this.width>0)&&(this.height>0)){ //verso destra e basso
            if ( (mouseX > this.x) && (mouseX < (this.width + this.x )) && (mouseY > this.y) && (mouseY < (this.height + this.y)) ) {
                return true;
            } else {
                return false;
            }
        } else if ((this.width>0)&&(this.height<0)){ //verso destra e alto
            if ( (mouseX > this.x) && (mouseX < (this.width + this.x )) && (mouseY < this.y) && (mouseY > (this.height + this.y)) ) {
                return true;
            } else {
                return false;
            }
        }else if ((this.width<0)&&(this.height>0)){ //verso sinistra e basso
            if ( (mouseX < this.x) && (mouseX > (this.width + this.x )) && (mouseY > this.y) && (mouseY < (this.height + this.y)) ) {
                return true;
            } else {
                return false;
            }
        }else if ((this.width<0)&&(this.height<0)){ //verso sinistra e alto
            if ( (mouseX < this.x) && (mouseX > (this.width + this.x )) && (mouseY < this.y) && (mouseY > (this.height + this.y)) ) {
                return true;
            } else {
                return false;
            }
        }    
    }
    
    // GET & SET
    getX (){
        return this.x;
    }
    setX (x) {
        this.x = x;
    }
    
    getY(){
        return this.y;
    }
    setY (y) {
        this.y = y;
    }
    
    getWidth(){
        return this.width;
    }
    setWidth (width){
        this.width = width;
    }
    
    getHeight(){
        return this.height;
    }
    setHeight (height) {
        this.height = height;
    }

    getColor(){
        return this.color;
    }
    setColor(color){
        this.color = color;
    }

    getEndX(){
        return this.endX;
    }
    setEndX(endX){
        this.endX = endX;
    }

    getEndY(){
        return this.endY;
    }
    setEndY(endY){
        this.endY = endY;
    }

    getBorder(){
        return this.border;
    }
    setBorder(border){
        this.border = border;
    }

    getBorderColor(){
        return this.borderColor;
    }
    setBorderColor(borderColor){
        this.borderColor = borderColor;
    }

    getFill(){
        return this.fill;
    }
    setFill(fill){
        this.fill = fill;
    }

    getRotate(){
        return this.rotate;
    }
    setRotate(rotate){
        this.rotate = rotate;
    }
}