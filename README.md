# Re-Paint1.0

(dicembre 2018 - marzo 2019)

Riproduzione web del famoso tool di Windows, con aggiunte le funzionalità di modifica, selezione e cancellazione, non implementate nel canonico Paint.

_Il progetto è stato presentato come prova finale del corso Programmazione d'Interfacce tenuto all'Università di Pisa._
